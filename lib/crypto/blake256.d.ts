/// <reference types="node" />
import { Buffer } from 'buffer';
declare class Blake256 {
    private _h;
    private _s;
    private _block;
    private _blockOffset;
    private _length;
    private _nullt;
    private _zo;
    private _oo;
    private _compress;
    private _padding;
    private static _length_carry;
    digest(encoding: BufferEncoding): string;
    update(data: string | Buffer, encoding?: BufferEncoding): this;
    private static sigma;
    private static u256;
    private static padding;
    private static _g;
    private static _rot;
}
export { Blake256 };
