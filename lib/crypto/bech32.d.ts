declare const encodings: {
    readonly BECH32: "bech32";
    readonly BECH32M: "bech32m";
};
declare type Encoding = (typeof encodings)[keyof typeof encodings];
declare function verifyChecksum(hrp: string, data: Uint8Array | Array<number>, enc: Encoding): boolean;
declare function encode(hrp: string, data: Array<number>, enc: Encoding): string;
declare function decode(bechString: string, enc: Encoding): {
    hrp: string;
    data: number[];
} | null;
declare const bech32: {
    decode: typeof decode;
    encode: typeof encode;
    encodings: {
        readonly BECH32: "bech32";
        readonly BECH32M: "bech32m";
    };
    verifyChecksum: typeof verifyChecksum;
};
export { bech32 };
export type { Encoding };
