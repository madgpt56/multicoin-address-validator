/// <reference types="node" />
declare class Blake2b {
    private readonly outlen;
    private readonly _b;
    private readonly _h;
    private _t;
    private _c;
    constructor(outlen: number);
    private blake2bCompress;
    private blake2bUpdate;
    private blake2bFinal;
    update(input: Buffer): this;
    digest(): string;
    final: () => string;
}
export { Blake2b };
