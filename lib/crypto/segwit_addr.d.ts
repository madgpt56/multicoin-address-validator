import { OptsNetworkTypeOptional } from '../types';
declare function decode(hrp: string, addr: string): {
    version: number;
    program: number[];
} | null;
declare function encode(hrp: string, version: number, program: Array<number>): string | null;
declare type NetworkType = 'prod' | 'testnet' | string;
declare function isValidAddress(address: string, currency: Record<string, any>, opts?: OptsNetworkTypeOptional<NetworkType>): boolean;
declare const segwit: {
    encode: typeof encode;
    decode: typeof decode;
    isValidAddress: typeof isValidAddress;
};
export { segwit };
