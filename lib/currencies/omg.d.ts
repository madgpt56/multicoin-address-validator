declare const omgCurrency: {
    readonly name: "OmiseGO";
    readonly symbol: "omg";
};
declare const omgValidate: (address: string) => boolean;
export { omgCurrency, omgValidate, };
