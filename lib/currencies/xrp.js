"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.xrpValidate = exports.xrpCurrency = void 0;
const ripple_validator_1 = require("../validators/ripple_validator");
const xrpCurrency = {
    name: 'Ripple',
    symbol: 'xrp',
};
exports.xrpCurrency = xrpCurrency;
const xrpValidate = ripple_validator_1.XRPValidator.isValidAddress;
exports.xrpValidate = xrpValidate;
//# sourceMappingURL=xrp.js.map