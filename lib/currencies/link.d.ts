declare const linkCurrency: {
    readonly name: "Chainlink";
    readonly symbol: "link";
};
declare const linkValidate: (address: string) => boolean;
export { linkCurrency, linkValidate, };
