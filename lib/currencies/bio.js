"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.bioValidate = exports.bioCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const bioCurrency = {
    name: 'BioCoin',
    symbol: 'bio',
    addressTypes: {
        prod: [
            '19',
            '14',
        ],
        testnet: [
            '6f',
            'c4',
        ],
    },
};
exports.bioCurrency = bioCurrency;
const bioValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, bioCurrency, opts);
exports.bioValidate = bioValidate;
//# sourceMappingURL=bio.js.map