declare const expCurrency: {
    readonly name: "Expanse";
    readonly symbol: "exp";
};
declare const expValidate: (address: string) => boolean;
export { expCurrency, expValidate, };
