declare const scCurrency: {
    readonly name: "Siacoin";
    readonly symbol: "sc";
};
declare const scValidate: (address: string) => boolean;
export { scCurrency, scValidate, };
