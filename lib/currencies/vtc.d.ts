import { ValidatorParams } from '../validators/bitcoin_validator';
declare const vtcCurrency: {
    readonly name: "VertCoin";
    readonly symbol: "vtc";
    readonly addressTypes: {
        readonly prod: readonly ["0x", "47", "71", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
};
declare const vtcValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { vtcCurrency, vtcValidate, };
