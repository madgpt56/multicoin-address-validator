"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.btcValidate = exports.btcCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const btcCurrency = {
    name: 'Bitcoin',
    symbol: 'btc',
    addressTypes: {
        prod: [
            '00',
            '05',
        ],
        testnet: [
            '6f',
            'c4',
            '3c',
            '26',
        ],
    },
    bech32Hrp: {
        prod: [
            'bc',
        ],
        testnet: [
            'tb',
        ],
    },
};
exports.btcCurrency = btcCurrency;
const btcValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, btcCurrency, opts);
exports.btcValidate = btcValidate;
//# sourceMappingURL=btc.js.map