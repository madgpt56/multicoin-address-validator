declare const spndCurrency: {
    readonly name: "Spendcoin";
    readonly symbol: "spnd";
};
declare const spndValidate: (address: string) => boolean;
export { spndCurrency, spndValidate, };
