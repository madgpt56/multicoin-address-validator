"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.spndValidate = exports.spndCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const spndCurrency = {
    name: 'Spendcoin',
    symbol: 'spnd',
};
exports.spndCurrency = spndCurrency;
const spndValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.spndValidate = spndValidate;
//# sourceMappingURL=spnd.js.map