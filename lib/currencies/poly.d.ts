declare const polyCurrency: {
    readonly name: "Polymath";
    readonly symbol: "poly";
};
declare const polyValidate: (address: string) => boolean;
export { polyCurrency, polyValidate, };
