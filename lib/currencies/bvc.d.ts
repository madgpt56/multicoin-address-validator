import { ValidatorParams } from '../validators/bitcoin_validator';
declare const bvcCurrency: {
    readonly name: "BeaverCoin";
    readonly symbol: "bvc";
    readonly addressTypes: {
        readonly prod: readonly ["19", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
};
declare const bvcValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { bvcCurrency, bvcValidate, };
