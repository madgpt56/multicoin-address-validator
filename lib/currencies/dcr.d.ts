import { ValidatorParams } from '../validators/bitcoin_validator';
declare const dcrCurrency: {
    readonly name: "Decred";
    readonly symbol: "dcr";
    readonly addressTypes: {
        readonly prod: readonly ["073f", "071a"];
        readonly testnet: readonly ["0f21", "0efc"];
    };
    readonly hashFunction: "blake256";
    readonly expectedLength: 26;
};
declare const dcrValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { dcrCurrency, dcrValidate, };
