"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sngValidate = exports.sngCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const sngCurrency = {
    name: 'SnowGem',
    symbol: 'sng',
    expectedLength: 26,
    addressTypes: {
        prod: [
            '1c28',
            '1c2d',
        ],
        testnet: [
            '1d25',
            '1cba',
        ],
    },
};
exports.sngCurrency = sngCurrency;
const sngValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, sngCurrency, opts);
exports.sngValidate = sngValidate;
//# sourceMappingURL=sng.js.map