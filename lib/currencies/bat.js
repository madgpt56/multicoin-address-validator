"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.batValidate = exports.batCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const batCurrency = {
    name: 'Basic Attention Token',
    symbol: 'bat',
};
exports.batCurrency = batCurrency;
const batValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.batValidate = batValidate;
//# sourceMappingURL=bat.js.map