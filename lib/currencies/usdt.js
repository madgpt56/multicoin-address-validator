"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.usdtValidate = exports.usdtCurrency = void 0;
const usdt_validator_1 = require("../validators/usdt_validator");
const usdtCurrency = {
    name: 'Tether',
    symbol: 'usdt',
    addressTypes: { prod: ['00', '05'], testnet: ['6f', 'c4'] },
};
exports.usdtCurrency = usdtCurrency;
const usdtValidate = (address, opts) => usdt_validator_1.USDTValidator.isValidAddress(address, usdtCurrency, opts);
exports.usdtValidate = usdtValidate;
//# sourceMappingURL=usdt.js.map