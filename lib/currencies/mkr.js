"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mkrValidate = exports.mkrCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const mkrCurrency = {
    name: 'Maker',
    symbol: 'mkr',
};
exports.mkrCurrency = mkrCurrency;
const mkrValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.mkrValidate = mkrValidate;
//# sourceMappingURL=mkr.js.map