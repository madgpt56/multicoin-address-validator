"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.repValidate = exports.repCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const repCurrency = {
    name: 'Augur',
    symbol: 'rep',
};
exports.repCurrency = repCurrency;
const repValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.repValidate = repValidate;
//# sourceMappingURL=rep.js.map