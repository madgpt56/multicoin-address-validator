"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dntValidate = exports.dntCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const dntCurrency = {
    name: 'District0x',
    symbol: 'dnt',
};
exports.dntCurrency = dntCurrency;
const dntValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.dntValidate = dntValidate;
//# sourceMappingURL=dnt.js.map