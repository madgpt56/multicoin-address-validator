import { ValidatorParams } from '../validators/bitcoin_validator';
declare const sngCurrency: {
    readonly name: "SnowGem";
    readonly symbol: "sng";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1c28", "1c2d"];
        readonly testnet: readonly ["1d25", "1cba"];
    };
};
declare const sngValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { sngCurrency, sngValidate, };
