declare const eosCurrency: {
    readonly name: "EOS";
    readonly symbol: "eos";
};
declare const eosValidate: (address: string) => boolean;
export { eosCurrency, eosValidate, };
