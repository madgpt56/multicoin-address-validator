import { ValidatorParams } from '../validators/bitcoin_validator';
declare const zenCurrency: {
    readonly name: "ZenCash";
    readonly symbol: "zen";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["2089", "2096"];
        readonly testnet: readonly ["2092", "2098"];
    };
};
declare const zenValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { zenCurrency, zenValidate, };
