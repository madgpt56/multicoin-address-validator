"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.vibValidate = exports.vibCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const vibCurrency = {
    name: 'Viberate',
    symbol: 'vib',
};
exports.vibCurrency = vibCurrency;
const vibValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.vibValidate = vibValidate;
//# sourceMappingURL=vib.js.map