import { ValidatorParams } from '../validators/usdt_validator';
declare const usdtCurrency: {
    readonly name: "Tether";
    readonly symbol: "usdt";
    readonly addressTypes: {
        readonly prod: readonly ["00", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
};
declare const usdtValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { usdtCurrency, usdtValidate, };
