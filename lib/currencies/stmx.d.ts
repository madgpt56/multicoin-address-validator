declare const stmxCurrency: {
    readonly name: "StormX";
    readonly symbol: "stmx";
};
declare const stmxValidate: (address: string) => boolean;
export { stmxCurrency, stmxValidate, };
