"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.hedgValidate = exports.hedgCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const hedgCurrency = {
    name: 'HedgeTrade',
    symbol: 'hedg',
};
exports.hedgCurrency = hedgCurrency;
const hedgValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.hedgValidate = hedgValidate;
//# sourceMappingURL=hedg.js.map