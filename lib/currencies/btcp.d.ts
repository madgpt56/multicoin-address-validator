import { ValidatorParams } from '../validators/bitcoin_validator';
declare const btcpCurrency: {
    readonly name: "BitcoinPrivate";
    readonly symbol: "btcp";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1325", "13af"];
        readonly testnet: readonly ["1957", "19e0"];
    };
};
declare const btcpValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { btcpCurrency, btcpValidate, };
