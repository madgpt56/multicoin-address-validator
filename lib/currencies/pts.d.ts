import { ValidatorParams } from '../validators/bitcoin_validator';
declare const ptsCurrency: {
    readonly name: "ProtoShares";
    readonly symbol: "pts";
    readonly addressTypes: {
        readonly prod: readonly ["38", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
};
declare const ptsValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { ptsCurrency, ptsValidate, };
