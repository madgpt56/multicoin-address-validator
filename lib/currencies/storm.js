"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.stormValidate = exports.stormCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const stormCurrency = {
    name: 'Storm',
    symbol: 'storm',
};
exports.stormCurrency = stormCurrency;
const stormValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.stormValidate = stormValidate;
//# sourceMappingURL=storm.js.map