declare const rlcCurrency: {
    readonly name: "iExec RLC";
    readonly symbol: "rlc";
};
declare const rlcValidate: (address: string) => boolean;
export { rlcCurrency, rlcValidate, };
