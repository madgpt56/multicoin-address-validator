declare const solveCurrency: {
    readonly name: "SOLVE";
    readonly symbol: "solve";
};
declare const solveValidate: (address: string) => boolean;
export { solveCurrency, solveValidate, };
