import { ValidatorParams } from '../validators/base58_validator';
declare const solCurrency: {
    readonly name: "Solana";
    readonly symbol: "sol";
    readonly maxLength: 44;
    readonly minLength: 43;
};
declare const solValidate: (address: ValidatorParams[0]) => boolean;
export { solCurrency, solValidate, };
