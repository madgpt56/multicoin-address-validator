"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.daiValidate = exports.daiCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const daiCurrency = {
    name: 'Multi-collateral DAI',
    symbol: 'dai',
};
exports.daiCurrency = daiCurrency;
const daiValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.daiValidate = daiValidate;
//# sourceMappingURL=dai.js.map