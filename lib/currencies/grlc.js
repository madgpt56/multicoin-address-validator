"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.grlcValidate = exports.grlcCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const grlcCurrency = {
    name: 'GarliCoin',
    symbol: 'grlc',
    addressTypes: {
        prod: [
            '26',
            '05',
        ],
        testnet: [
            '6f',
            'c4',
        ],
    },
};
exports.grlcCurrency = grlcCurrency;
const grlcValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, grlcCurrency, opts);
exports.grlcValidate = grlcValidate;
//# sourceMappingURL=grlc.js.map