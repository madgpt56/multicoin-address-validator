declare const algoCurrency: {
    readonly name: "Algorand";
    readonly symbol: "algo";
};
declare const algoValidate: (address: string) => boolean;
export { algoCurrency, algoValidate, };
