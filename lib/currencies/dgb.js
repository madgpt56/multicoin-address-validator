"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dgbValidate = exports.dgbCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const dgbCurrency = {
    name: 'DigiByte',
    symbol: 'dgb',
    addressTypes: {
        prod: [
            '1e',
            '3f',
        ],
        testnet: [],
    },
    bech32Hrp: {
        prod: [
            'dgb',
            'S',
        ],
        testnet: [],
    },
};
exports.dgbCurrency = dgbCurrency;
const dgbValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, dgbCurrency, opts);
exports.dgbValidate = dgbValidate;
//# sourceMappingURL=dgb.js.map