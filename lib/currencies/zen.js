"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.zenValidate = exports.zenCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const zenCurrency = {
    name: 'ZenCash',
    symbol: 'zen',
    expectedLength: 26,
    addressTypes: {
        prod: [
            '2089',
            '2096',
        ],
        testnet: [
            '2092',
            '2098',
        ],
    },
};
exports.zenCurrency = zenCurrency;
const zenValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, zenCurrency, opts);
exports.zenValidate = zenValidate;
//# sourceMappingURL=zen.js.map