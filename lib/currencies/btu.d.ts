declare const btuCurrency: {
    readonly name: "BTU Protocol";
    readonly symbol: "btu";
};
declare const btuValidate: (address: string) => boolean;
export { btuCurrency, btuValidate, };
