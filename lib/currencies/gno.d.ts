declare const gnoCurrency: {
    readonly name: "Gnosis";
    readonly symbol: "gno";
};
declare const gnoValidate: (address: string) => boolean;
export { gnoCurrency, gnoValidate, };
