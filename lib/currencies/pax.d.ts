declare const paxCurrency: {
    readonly name: "Paxos";
    readonly symbol: "pax";
};
declare const paxValidate: (address: string) => boolean;
export { paxCurrency, paxValidate, };
