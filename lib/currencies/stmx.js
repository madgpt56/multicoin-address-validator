"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.stmxValidate = exports.stmxCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const stmxCurrency = {
    name: 'StormX',
    symbol: 'stmx',
};
exports.stmxCurrency = stmxCurrency;
const stmxValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.stmxValidate = stmxValidate;
//# sourceMappingURL=stmx.js.map