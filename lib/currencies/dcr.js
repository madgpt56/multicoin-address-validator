"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dcrValidate = exports.dcrCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const dcrCurrency = {
    name: 'Decred',
    symbol: 'dcr',
    addressTypes: {
        prod: [
            '073f',
            '071a',
        ],
        testnet: [
            '0f21',
            '0efc',
        ],
    },
    hashFunction: 'blake256',
    expectedLength: 26,
};
exports.dcrCurrency = dcrCurrency;
const dcrValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, dcrCurrency, opts);
exports.dcrValidate = dcrValidate;
//# sourceMappingURL=dcr.js.map