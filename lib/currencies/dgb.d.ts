import { ValidatorParams } from '../validators/bitcoin_validator';
declare const dgbCurrency: {
    readonly name: "DigiByte";
    readonly symbol: "dgb";
    readonly addressTypes: {
        readonly prod: readonly ["1e", "3f"];
        readonly testnet: readonly [];
    };
    readonly bech32Hrp: {
        readonly prod: readonly ["dgb", "S"];
        readonly testnet: readonly [];
    };
};
declare const dgbValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { dgbCurrency, dgbValidate, };
