"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.linkValidate = exports.linkCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const linkCurrency = {
    name: 'Chainlink',
    symbol: 'link',
};
exports.linkCurrency = linkCurrency;
const linkValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.linkValidate = linkValidate;
//# sourceMappingURL=link.js.map