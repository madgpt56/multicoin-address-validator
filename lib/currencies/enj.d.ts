declare const enjCurrency: {
    readonly name: "Enjin Coin";
    readonly symbol: "enj";
};
declare const enjValidate: (address: string) => boolean;
export { enjCurrency, enjValidate, };
