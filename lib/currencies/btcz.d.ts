import { ValidatorParams } from '../validators/bitcoin_validator';
declare const btczCurrency: {
    readonly name: "BitcoinZ";
    readonly symbol: "btcz";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1cb8", "1cbd"];
        readonly testnet: readonly ["1d25", "1cba"];
    };
};
declare const btczValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { btczCurrency, btczValidate, };
