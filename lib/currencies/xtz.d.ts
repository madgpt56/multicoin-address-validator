declare const xtzCurrency: {
    readonly name: "Tezos";
    readonly symbol: "xtz";
};
declare const xtzValidate: (address: string) => boolean;
export { xtzCurrency, xtzValidate, };
