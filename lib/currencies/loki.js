"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.lokiValidate = exports.lokiCurrency = void 0;
const monero_validator_1 = require("../validators/monero_validator");
const lokiCurrency = {
    name: 'loki',
    symbol: 'loki',
    addressTypes: { prod: ['114', '115', '116'], testnet: [] },
    iAddressTypes: { prod: ['115'], testnet: [] },
};
exports.lokiCurrency = lokiCurrency;
const lokiValidate = (address, opts) => monero_validator_1.XMRValidator.isValidAddress(address, lokiCurrency, opts);
exports.lokiValidate = lokiValidate;
//# sourceMappingURL=loki.js.map