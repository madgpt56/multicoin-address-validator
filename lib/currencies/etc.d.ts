declare const etcCurrency: {
    readonly name: "EthereumClassic";
    readonly symbol: "etc";
};
declare const etcValidate: (address: string) => boolean;
export { etcCurrency, etcValidate, };
