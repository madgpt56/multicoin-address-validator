"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.lskValidate = exports.lskCurrency = void 0;
const lisk_validator_1 = require("../validators/lisk_validator");
const lskCurrency = {
    name: 'Lisk',
    symbol: 'lsk',
};
exports.lskCurrency = lskCurrency;
const lskValidate = lisk_validator_1.LSKValidator.isValidAddress;
exports.lskValidate = lskValidate;
//# sourceMappingURL=lsk.js.map