"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.aaveValidate = exports.aaveCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const aaveCurrency = {
    name: 'Aave Coin',
    symbol: 'aave',
};
exports.aaveCurrency = aaveCurrency;
const aaveValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.aaveValidate = aaveValidate;
//# sourceMappingURL=aave.js.map