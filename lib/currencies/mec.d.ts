import { ValidatorParams } from '../validators/bitcoin_validator';
declare const mecCurrency: {
    readonly name: "MegaCoin";
    readonly symbol: "mec";
    readonly addressTypes: {
        readonly prod: readonly ["32", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
};
declare const mecValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { mecCurrency, mecValidate, };
