import { ValidatorParams } from '../validators/bitcoin_validator';
declare const votCurrency: {
    readonly name: "VoteCoin";
    readonly symbol: "vot";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1cb8", "1cbd"];
        readonly testnet: readonly ["1d25", "1cba"];
    };
};
declare const votValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { votCurrency, votValidate, };
