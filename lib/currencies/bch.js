"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.bchValidate = exports.bchCurrency = void 0;
const bch_validator_1 = require("../validators/bch_validator");
const bchCurrency = {
    name: 'BitcoinCash',
    symbol: 'bch',
    regexp: '^[qQpP]{1}[0-9a-zA-Z]{41}$',
    addressTypes: { prod: ['00', '05'], testnet: ['6f', 'c4'] },
};
exports.bchCurrency = bchCurrency;
const bchValidate = (address, opts) => bch_validator_1.BCHValidator.isValidAddress(address, bchCurrency, opts);
exports.bchValidate = bchValidate;
//# sourceMappingURL=bch.js.map