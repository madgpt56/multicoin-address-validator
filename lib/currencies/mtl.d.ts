declare const mtlCurrency: {
    readonly name: "Metal";
    readonly symbol: "mtl";
};
declare const mtlValidate: (address: string) => boolean;
export { mtlCurrency, mtlValidate, };
