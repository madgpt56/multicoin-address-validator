declare const qntCurrency: {
    readonly name: "Quant";
    readonly symbol: "qnt";
};
declare const qntValidate: (address: string) => boolean;
export { qntCurrency, qntValidate, };
