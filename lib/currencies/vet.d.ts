declare const vetCurrency: {
    readonly name: "VeChain";
    readonly symbol: "vet";
};
declare const vetValidate: (address: string) => boolean;
export { vetCurrency, vetValidate, };
