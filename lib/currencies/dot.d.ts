declare const dotCurrency: {
    readonly name: "Polkadot";
    readonly symbol: "dot";
};
declare const dotValidate: (address: string) => boolean;
export { dotCurrency, dotValidate, };
