declare const payCurrency: {
    readonly name: "TenX";
    readonly symbol: "pay";
};
declare const payValidate: (address: string) => boolean;
export { payCurrency, payValidate, };
