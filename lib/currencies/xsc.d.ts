declare const xscCurrency: {
    readonly name: "HyperSpace";
    readonly symbol: "xsc";
};
declare const xscValidate: (address: string) => boolean;
export { xscCurrency, xscValidate, };
