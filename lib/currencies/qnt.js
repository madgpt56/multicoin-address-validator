"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.qntValidate = exports.qntCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const qntCurrency = {
    name: 'Quant',
    symbol: 'qnt',
};
exports.qntCurrency = qntCurrency;
const qntValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.qntValidate = qntValidate;
//# sourceMappingURL=qnt.js.map