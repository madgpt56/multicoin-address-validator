import { ValidatorParams } from '../validators/ada_validator';
declare const adaCurrency: {
    readonly name: "Cardano";
    readonly symbol: "ada";
    readonly bech32Hrp: {
        readonly prod: readonly ["addr"];
        readonly testnet: readonly ["addr"];
    };
};
declare const adaValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { adaCurrency, adaValidate, };
