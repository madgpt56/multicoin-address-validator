declare const ethCurrency: {
    readonly name: "Ethereum";
    readonly symbol: "eth";
};
declare const ethValidate: (address: string) => boolean;
export { ethCurrency, ethValidate, };
