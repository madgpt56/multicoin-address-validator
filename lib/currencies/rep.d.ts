declare const repCurrency: {
    readonly name: "Augur";
    readonly symbol: "rep";
};
declare const repValidate: (address: string) => boolean;
export { repCurrency, repValidate, };
