declare const cloCurrency: {
    readonly name: "Callisto";
    readonly symbol: "clo";
};
declare const cloValidate: (address: string) => boolean;
export { cloCurrency, cloValidate, };
