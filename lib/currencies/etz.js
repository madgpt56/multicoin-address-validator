"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.etzValidate = exports.etzCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const etzCurrency = {
    name: 'EtherZero',
    symbol: 'etz',
};
exports.etzCurrency = etzCurrency;
const etzValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.etzValidate = etzValidate;
//# sourceMappingURL=etz.js.map