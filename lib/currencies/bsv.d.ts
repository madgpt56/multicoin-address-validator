import { ValidatorParams } from '../validators/bch_validator';
declare const bsvCurrency: {
    readonly name: "Bitcoin SV";
    readonly symbol: "bsv";
    readonly regexp: "^[qQ]{1}[0-9a-zA-Z]{41}$";
    readonly addressTypes: {
        readonly prod: readonly ["00", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
};
declare const bsvValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { bsvCurrency, bsvValidate, };
