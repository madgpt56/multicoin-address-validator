"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.nanoValidate = exports.nanoCurrency = void 0;
const nano_validator_1 = require("../validators/nano_validator");
const nanoCurrency = {
    name: 'Nano',
    symbol: 'nano',
};
exports.nanoCurrency = nanoCurrency;
const nanoValidate = nano_validator_1.NANOValidator.isValidAddress;
exports.nanoValidate = nanoValidate;
//# sourceMappingURL=nano.js.map