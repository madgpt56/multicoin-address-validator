declare const xtpCurrency: {
    readonly name: "Tap";
    readonly symbol: "xtp";
};
declare const xtpValidate: (address: string) => boolean;
export { xtpCurrency, xtpValidate, };
