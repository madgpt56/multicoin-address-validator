"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pivxValidate = exports.pivxCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const pivxCurrency = {
    name: 'PIVX',
    symbol: 'pivx',
    addressTypes: {
        prod: [
            '1e',
            '0d',
        ],
        testnet: [],
    },
};
exports.pivxCurrency = pivxCurrency;
const pivxValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, pivxCurrency, opts);
exports.pivxValidate = pivxValidate;
//# sourceMappingURL=pivx.js.map