"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cvcValidate = exports.cvcCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const cvcCurrency = {
    name: 'Civic',
    symbol: 'cvc',
};
exports.cvcCurrency = cvcCurrency;
const cvcValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.cvcValidate = cvcValidate;
//# sourceMappingURL=cvc.js.map