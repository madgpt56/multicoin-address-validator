declare const gntCurrency: {
    readonly name: "Golem (GNT)";
    readonly symbol: "gnt";
};
declare const gntValidate: (address: string) => boolean;
export { gntCurrency, gntValidate, };
