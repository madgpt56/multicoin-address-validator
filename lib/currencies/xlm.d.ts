declare const xlmCurrency: {
    readonly name: "Stellar";
    readonly symbol: "xlm";
};
declare const xlmValidate: (address: string) => boolean;
export { xlmCurrency, xlmValidate, };
