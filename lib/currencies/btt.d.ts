declare const bttCurrency: {
    readonly name: "BlockTrade";
    readonly symbol: "btt";
};
declare const bttValidate: (address: string) => boolean;
export { bttCurrency, bttValidate, };
