declare const mkrCurrency: {
    readonly name: "Maker";
    readonly symbol: "mkr";
};
declare const mkrValidate: (address: string) => boolean;
export { mkrCurrency, mkrValidate, };
