"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.swtValidate = exports.swtCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const swtCurrency = {
    name: 'Swarm City',
    symbol: 'swt',
};
exports.swtCurrency = swtCurrency;
const swtValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.swtValidate = swtValidate;
//# sourceMappingURL=swt.js.map