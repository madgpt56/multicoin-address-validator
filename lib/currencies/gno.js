"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.gnoValidate = exports.gnoCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const gnoCurrency = {
    name: 'Gnosis',
    symbol: 'gno',
};
exports.gnoCurrency = gnoCurrency;
const gnoValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.gnoValidate = gnoValidate;
//# sourceMappingURL=gno.js.map