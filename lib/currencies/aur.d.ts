import { ValidatorParams } from '../validators/bitcoin_validator';
declare const aurCurrency: {
    readonly name: "AuroraCoin";
    readonly symbol: "aur";
    readonly addressTypes: {
        readonly prod: readonly ["17", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
};
declare const aurValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { aurCurrency, aurValidate, };
