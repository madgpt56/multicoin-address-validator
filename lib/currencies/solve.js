"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.solveValidate = exports.solveCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const solveCurrency = {
    name: 'SOLVE',
    symbol: 'solve',
};
exports.solveCurrency = solveCurrency;
const solveValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.solveValidate = solveValidate;
//# sourceMappingURL=solve.js.map