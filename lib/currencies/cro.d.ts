import { ValidatorParams } from '../validators/bip173_validator';
declare const croCurrency: {
    readonly name: "Crypto.com Coin";
    readonly symbol: "cro";
    readonly bech32Hrp: {
        readonly prod: readonly ["cro"];
        readonly testnet: readonly ["tcro"];
    };
};
declare const croValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { croCurrency, croValidate, };
