"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.omgValidate = exports.omgCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const omgCurrency = {
    name: 'OmiseGO',
    symbol: 'omg',
};
exports.omgCurrency = omgCurrency;
const omgValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.omgValidate = omgValidate;
//# sourceMappingURL=omg.js.map