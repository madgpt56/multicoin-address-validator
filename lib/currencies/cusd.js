"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cusdValidate = exports.cusdCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const cusdCurrency = {
    name: 'CUSD',
    symbol: 'cusd',
};
exports.cusdCurrency = cusdCurrency;
const cusdValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.cusdValidate = cusdValidate;
//# sourceMappingURL=cusd.js.map