"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.payValidate = exports.payCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const payCurrency = {
    name: 'TenX',
    symbol: 'pay',
};
exports.payCurrency = payCurrency;
const payValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.payValidate = payValidate;
//# sourceMappingURL=pay.js.map