"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.manaValidate = exports.manaCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const manaCurrency = {
    name: 'Decentraland',
    symbol: 'mana',
};
exports.manaCurrency = manaCurrency;
const manaValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.manaValidate = manaValidate;
//# sourceMappingURL=mana.js.map