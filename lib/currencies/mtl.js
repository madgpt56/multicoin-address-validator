"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mtlValidate = exports.mtlCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const mtlCurrency = {
    name: 'Metal',
    symbol: 'mtl',
};
exports.mtlCurrency = mtlCurrency;
const mtlValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.mtlValidate = mtlValidate;
//# sourceMappingURL=mtl.js.map