"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.paxValidate = exports.paxCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const paxCurrency = {
    name: 'Paxos',
    symbol: 'pax',
};
exports.paxCurrency = paxCurrency;
const paxValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.paxValidate = paxValidate;
//# sourceMappingURL=pax.js.map