import { ValidatorParams } from '../validators/bitcoin_validator';
declare const ppcCurrency: {
    readonly name: "PeerCoin";
    readonly symbol: "ppc";
    readonly addressTypes: {
        readonly prod: readonly ["37", "75"];
        readonly testnet: readonly ["6f", "c4"];
    };
};
declare const ppcValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { ppcCurrency, ppcValidate, };
