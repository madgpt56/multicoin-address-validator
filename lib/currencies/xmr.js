"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.xmrValidate = exports.xmrCurrency = void 0;
const monero_validator_1 = require("../validators/monero_validator");
const xmrCurrency = {
    name: 'Monero',
    symbol: 'xmr',
    addressTypes: { prod: ['18', '42'], testnet: ['53', '63'], stagenet: ['24'] },
    iAddressTypes: { prod: ['19'], testnet: ['54'], stagenet: ['25'] },
};
exports.xmrCurrency = xmrCurrency;
const xmrValidate = (address, opts) => monero_validator_1.XMRValidator.isValidAddress(address, xmrCurrency, opts);
exports.xmrValidate = xmrValidate;
//# sourceMappingURL=xmr.js.map