"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.kmdValidate = exports.kmdCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const kmdCurrency = {
    name: 'Komodo',
    symbol: 'kmd',
    addressTypes: {
        prod: [
            '3c',
            '55',
        ],
        testnet: [
            '0',
            '5',
        ],
    },
};
exports.kmdCurrency = kmdCurrency;
const kmdValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, kmdCurrency, opts);
exports.kmdValidate = kmdValidate;
//# sourceMappingURL=kmd.js.map