"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.btczValidate = exports.btczCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const btczCurrency = {
    name: 'BitcoinZ',
    symbol: 'btcz',
    expectedLength: 26,
    addressTypes: {
        prod: [
            '1cb8',
            '1cbd',
        ],
        testnet: [
            '1d25',
            '1cba',
        ],
    },
};
exports.btczCurrency = btczCurrency;
const btczValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, btczCurrency, opts);
exports.btczValidate = btczValidate;
//# sourceMappingURL=btcz.js.map