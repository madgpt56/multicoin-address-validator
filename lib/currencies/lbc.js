"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.lbcValidate = exports.lbcCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const lbcCurrency = {
    name: 'LBRY Credits',
    symbol: 'lbc',
    addressTypes: {
        prod: [
            '55',
        ],
        testnet: [],
    },
};
exports.lbcCurrency = lbcCurrency;
const lbcValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, lbcCurrency, opts);
exports.lbcValidate = lbcValidate;
//# sourceMappingURL=lbc.js.map