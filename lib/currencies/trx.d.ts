import { ValidatorParams } from '../validators/tron_validator';
declare const trxCurrency: {
    readonly name: "Tron";
    readonly symbol: "trx";
    readonly addressTypes: {
        readonly prod: readonly [65];
        readonly testnet: readonly [160];
    };
};
declare const trxValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { trxCurrency, trxValidate, };
