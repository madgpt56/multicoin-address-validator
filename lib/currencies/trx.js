"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.trxValidate = exports.trxCurrency = void 0;
const tron_validator_1 = require("../validators/tron_validator");
const trxCurrency = {
    name: 'Tron',
    symbol: 'trx',
    addressTypes: { prod: [0x41], testnet: [0xa0] },
};
exports.trxCurrency = trxCurrency;
const trxValidate = (address, opts) => tron_validator_1.TRXValidator.isValidAddress(address, trxCurrency, opts);
exports.trxValidate = trxValidate;
//# sourceMappingURL=trx.js.map