"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.usdcValidate = exports.usdcCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const usdcCurrency = {
    name: 'USD Coin',
    symbol: 'usdc',
};
exports.usdcCurrency = usdcCurrency;
const usdcValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.usdcValidate = usdcValidate;
//# sourceMappingURL=usdc.js.map