declare const daiCurrency: {
    readonly name: "Multi-collateral DAI";
    readonly symbol: "dai";
};
declare const daiValidate: (address: string) => boolean;
export { daiCurrency, daiValidate, };
