"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.maticValidate = exports.maticCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const maticCurrency = {
    name: 'Matic',
    symbol: 'matic',
};
exports.maticCurrency = maticCurrency;
const maticValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.maticValidate = maticValidate;
//# sourceMappingURL=matic.js.map