import { ValidatorParams } from '../validators/bitcoin_validator';
declare const slrCurrency: {
    readonly name: "SolarCoin";
    readonly symbol: "slr";
    readonly addressTypes: {
        readonly prod: readonly ["12", "05"];
        readonly testnet: readonly [];
    };
};
declare const slrValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { slrCurrency, slrValidate, };
