declare const storjCurrency: {
    readonly name: "Storj";
    readonly symbol: "storj";
};
declare const storjValidate: (address: string) => boolean;
export { storjCurrency, storjValidate, };
