declare const stormCurrency: {
    readonly name: "Storm";
    readonly symbol: "storm";
};
declare const stormValidate: (address: string) => boolean;
export { stormCurrency, stormValidate, };
