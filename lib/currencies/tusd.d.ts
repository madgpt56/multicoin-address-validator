declare const tusdCurrency: {
    readonly name: "TrueUSD";
    readonly symbol: "tusd";
};
declare const tusdValidate: (address: string) => boolean;
export { tusdCurrency, tusdValidate, };
