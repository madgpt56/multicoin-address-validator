import { ValidatorParams } from '../validators/bitcoin_validator';
declare const ltcCurrency: {
    readonly name: "LiteCoin";
    readonly symbol: "ltc";
    readonly addressTypes: {
        readonly prod: readonly ["30", "05", "32"];
        readonly testnet: readonly ["6f", "c4", "3a"];
    };
    readonly bech32Hrp: {
        readonly prod: readonly ["ltc"];
        readonly testnet: readonly ["tltc"];
    };
};
declare const ltcValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { ltcCurrency, ltcValidate, };
