import { ValidatorParams } from '../validators/monero_validator';
declare const lokiCurrency: {
    readonly name: "loki";
    readonly symbol: "loki";
    readonly addressTypes: {
        readonly prod: readonly ["114", "115", "116"];
        readonly testnet: readonly [];
    };
    readonly iAddressTypes: {
        readonly prod: readonly ["115"];
        readonly testnet: readonly [];
    };
};
declare const lokiValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { lokiCurrency, lokiValidate, };
