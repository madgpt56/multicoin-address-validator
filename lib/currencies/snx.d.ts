declare const snxCurrency: {
    readonly name: "Synthetix Network";
    readonly symbol: "snx";
};
declare const snxValidate: (address: string) => boolean;
export { snxCurrency, snxValidate, };
