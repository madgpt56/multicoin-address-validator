"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.gameValidate = exports.gameCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const gameCurrency = {
    name: 'GameCredits',
    symbol: 'game',
    addressTypes: { prod: ['26', '05'], testnet: [] },
};
exports.gameCurrency = gameCurrency;
const gameValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.gameValidate = gameValidate;
//# sourceMappingURL=game.js.map