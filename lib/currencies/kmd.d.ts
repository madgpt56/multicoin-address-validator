import { ValidatorParams } from '../validators/bitcoin_validator';
declare const kmdCurrency: {
    readonly name: "Komodo";
    readonly symbol: "kmd";
    readonly addressTypes: {
        readonly prod: readonly ["3c", "55"];
        readonly testnet: readonly ["0", "5"];
    };
};
declare const kmdValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { kmdCurrency, kmdValidate, };
