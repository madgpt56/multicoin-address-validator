declare const sntCurrency: {
    readonly name: "Status";
    readonly symbol: "snt";
};
declare const sntValidate: (address: string) => boolean;
export { sntCurrency, sntValidate, };
