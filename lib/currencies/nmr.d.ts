declare const nmrCurrency: {
    readonly name: "Numeraire";
    readonly symbol: "nmr";
};
declare const nmrValidate: (address: string) => boolean;
export { nmrCurrency, nmrValidate, };
