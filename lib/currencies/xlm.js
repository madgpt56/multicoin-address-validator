"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.xlmValidate = exports.xlmCurrency = void 0;
const stellar_validator_1 = require("../validators/stellar_validator");
const xlmCurrency = {
    name: 'Stellar',
    symbol: 'xlm',
};
exports.xlmCurrency = xlmCurrency;
const xlmValidate = stellar_validator_1.XLMValidator.isValidAddress;
exports.xlmValidate = xlmValidate;
//# sourceMappingURL=xlm.js.map