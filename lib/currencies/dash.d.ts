import { ValidatorParams } from '../validators/bitcoin_validator';
declare const dashCurrency: {
    readonly name: "Dash";
    readonly symbol: "dash";
    readonly addressTypes: {
        readonly prod: readonly ["4c", "10"];
        readonly testnet: readonly ["8c", "13"];
    };
};
declare const dashValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { dashCurrency, dashValidate, };
