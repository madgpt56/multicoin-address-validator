declare const aaveCurrency: {
    readonly name: "Aave Coin";
    readonly symbol: "aave";
};
declare const aaveValidate: (address: string) => boolean;
export { aaveCurrency, aaveValidate, };
