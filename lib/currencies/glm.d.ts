declare const glmCurrency: {
    readonly name: "Golem";
    readonly symbol: "glm";
};
declare const glmValidate: (address: string) => boolean;
export { glmCurrency, glmValidate, };
