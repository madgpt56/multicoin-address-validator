declare const lbaCurrency: {
    readonly name: "Cred";
    readonly symbol: "lba";
};
declare const lbaValidate: (address: string) => boolean;
export { lbaCurrency, lbaValidate, };
