"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.expValidate = exports.expCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const expCurrency = {
    name: 'Expanse',
    symbol: 'exp',
};
exports.expCurrency = expCurrency;
const expValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.expValidate = expValidate;
//# sourceMappingURL=exp.js.map