declare const usdcCurrency: {
    readonly name: "USD Coin";
    readonly symbol: "usdc";
};
declare const usdcValidate: (address: string) => boolean;
export { usdcCurrency, usdcValidate, };
