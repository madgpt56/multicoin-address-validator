"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.uniValidate = exports.uniCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const uniCurrency = {
    name: 'Uniswap Coin',
    symbol: 'uni',
};
exports.uniCurrency = uniCurrency;
const uniValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.uniValidate = uniValidate;
//# sourceMappingURL=uni.js.map