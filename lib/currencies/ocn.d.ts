declare const ocnCurrency: {
    readonly name: "Odyssey";
    readonly symbol: "ocn";
};
declare const ocnValidate: (address: string) => boolean;
export { ocnCurrency, ocnValidate, };
