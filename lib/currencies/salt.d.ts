declare const saltCurrency: {
    readonly name: "Salt";
    readonly symbol: "salt";
};
declare const saltValidate: (address: string) => boolean;
export { saltCurrency, saltValidate, };
