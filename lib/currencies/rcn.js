"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.rcnValidate = exports.rcnCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const rcnCurrency = {
    name: 'Ripio Credit Network',
    symbol: 'rcn',
};
exports.rcnCurrency = rcnCurrency;
const rcnValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.rcnValidate = rcnValidate;
//# sourceMappingURL=rcn.js.map