import { ValidatorParams } from '../validators/bitcoin_validator';
declare const zecCurrency: {
    readonly name: "ZCash";
    readonly symbol: "zec";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1cb8", "1cbd"];
        readonly testnet: readonly ["1d25", "1cba"];
    };
};
declare const zecValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { zecCurrency, zecValidate, };
