declare const vibCurrency: {
    readonly name: "Viberate";
    readonly symbol: "vib";
};
declare const vibValidate: (address: string) => boolean;
export { vibCurrency, vibValidate, };
