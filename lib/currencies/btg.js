"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.btgValidate = exports.btgCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const btgCurrency = {
    name: 'BitcoinGold',
    symbol: 'btg',
    addressTypes: {
        prod: [
            '26',
            '17',
        ],
        testnet: [
            '6f',
            'c4',
        ],
    },
};
exports.btgCurrency = btgCurrency;
const btgValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, btgCurrency, opts);
exports.btgValidate = btgValidate;
//# sourceMappingURL=btg.js.map