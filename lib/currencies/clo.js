"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cloValidate = exports.cloCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const cloCurrency = {
    name: 'Callisto',
    symbol: 'clo',
};
exports.cloCurrency = cloCurrency;
const cloValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.cloValidate = cloValidate;
//# sourceMappingURL=clo.js.map