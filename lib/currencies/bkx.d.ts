declare const bkxCurrency: {
    readonly name: "Bankex";
    readonly symbol: "bkx";
};
declare const bkxValidate: (address: string) => boolean;
export { bkxCurrency, bkxValidate, };
