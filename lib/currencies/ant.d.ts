declare const antCurrency: {
    readonly name: "Aragon";
    readonly symbol: "ant";
};
declare const antValidate: (address: string) => boolean;
export { antCurrency, antValidate, };
