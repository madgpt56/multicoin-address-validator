import { ValidatorParams } from '../validators/bitcoin_validator';
declare const nmcCurrency: {
    readonly name: "NameCoin";
    readonly symbol: "nmc";
    readonly addressTypes: {
        readonly prod: readonly ["34"];
        readonly testnet: readonly [];
    };
};
declare const nmcValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { nmcCurrency, nmcValidate, };
