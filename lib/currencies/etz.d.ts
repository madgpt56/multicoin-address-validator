declare const etzCurrency: {
    readonly name: "EtherZero";
    readonly symbol: "etz";
};
declare const etzValidate: (address: string) => boolean;
export { etzCurrency, etzValidate, };
