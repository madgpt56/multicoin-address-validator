declare const maticCurrency: {
    readonly name: "Matic";
    readonly symbol: "matic";
};
declare const maticValidate: (address: string) => boolean;
export { maticCurrency, maticValidate, };
