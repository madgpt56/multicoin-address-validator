"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sntValidate = exports.sntCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const sntCurrency = {
    name: 'Status',
    symbol: 'snt',
};
exports.sntCurrency = sntCurrency;
const sntValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.sntValidate = sntValidate;
//# sourceMappingURL=snt.js.map