declare const gupCurrency: {
    readonly name: "Matchpool";
    readonly symbol: "gup";
};
declare const gupValidate: (address: string) => boolean;
export { gupCurrency, gupValidate, };
