import { ValidatorParams } from '../validators/bitcoin_validator';
declare const wavesCurrency: {
    readonly name: "Waves";
    readonly symbol: "waves";
    readonly addressTypes: {
        readonly prod: readonly ["0157"];
        readonly testnet: readonly ["0154"];
    };
    readonly expectedLength: 26;
    readonly hashFunction: "blake256keccak256";
    readonly regex: RegExp;
};
declare const wavesValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { wavesCurrency, wavesValidate, };
