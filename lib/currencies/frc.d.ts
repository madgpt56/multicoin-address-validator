import { ValidatorParams } from '../validators/bitcoin_validator';
declare const frcCurrency: {
    readonly name: "FreiCoin";
    readonly symbol: "frc";
    readonly addressTypes: {
        readonly prod: readonly ["00", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
};
declare const frcValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { frcCurrency, frcValidate, };
