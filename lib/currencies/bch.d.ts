import { ValidatorParams } from '../validators/bch_validator';
declare const bchCurrency: {
    readonly name: "BitcoinCash";
    readonly symbol: "bch";
    readonly regexp: "^[qQpP]{1}[0-9a-zA-Z]{41}$";
    readonly addressTypes: {
        readonly prod: readonly ["00", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
};
declare const bchValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { bchCurrency, bchValidate, };
