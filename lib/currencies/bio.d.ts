import { ValidatorParams } from '../validators/bitcoin_validator';
declare const bioCurrency: {
    readonly name: "BioCoin";
    readonly symbol: "bio";
    readonly addressTypes: {
        readonly prod: readonly ["19", "14"];
        readonly testnet: readonly ["6f", "c4"];
    };
};
declare const bioValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { bioCurrency, bioValidate, };
