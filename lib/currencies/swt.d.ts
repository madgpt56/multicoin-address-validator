declare const swtCurrency: {
    readonly name: "Swarm City";
    readonly symbol: "swt";
};
declare const swtValidate: (address: string) => boolean;
export { swtCurrency, swtValidate, };
