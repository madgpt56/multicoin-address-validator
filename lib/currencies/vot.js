"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.votValidate = exports.votCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const votCurrency = {
    name: 'VoteCoin',
    symbol: 'vot',
    expectedLength: 26,
    addressTypes: {
        prod: [
            '1cb8',
            '1cbd',
        ],
        testnet: [
            '1d25',
            '1cba',
        ],
    },
};
exports.votCurrency = votCurrency;
const votValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, votCurrency, opts);
exports.votValidate = votValidate;
//# sourceMappingURL=vot.js.map