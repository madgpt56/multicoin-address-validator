declare const rcnCurrency: {
    readonly name: "Ripio Credit Network";
    readonly symbol: "rcn";
};
declare const rcnValidate: (address: string) => boolean;
export { rcnCurrency, rcnValidate, };
