"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ethValidate = exports.ethCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const ethCurrency = {
    name: 'Ethereum',
    symbol: 'eth',
};
exports.ethCurrency = ethCurrency;
const ethValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.ethValidate = ethValidate;
//# sourceMappingURL=eth.js.map