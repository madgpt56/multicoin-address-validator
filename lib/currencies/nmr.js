"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.nmrValidate = exports.nmrCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const nmrCurrency = {
    name: 'Numeraire',
    symbol: 'nmr',
};
exports.nmrCurrency = nmrCurrency;
const nmrValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.nmrValidate = nmrValidate;
//# sourceMappingURL=nmr.js.map