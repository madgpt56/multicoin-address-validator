"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.servValidate = exports.servCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const servCurrency = {
    name: 'Serve',
    symbol: 'serv',
};
exports.servCurrency = servCurrency;
const servValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.servValidate = servValidate;
//# sourceMappingURL=serv.js.map