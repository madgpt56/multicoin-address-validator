"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.zrxValidate = exports.zrxCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const zrxCurrency = {
    name: '0x',
    symbol: 'zrx',
};
exports.zrxCurrency = zrxCurrency;
const zrxValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.zrxValidate = zrxValidate;
//# sourceMappingURL=zrx.js.map