"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.algoValidate = exports.algoCurrency = void 0;
const algo_validator_1 = require("../validators/algo_validator");
const algoCurrency = {
    name: 'Algorand',
    symbol: 'algo',
};
exports.algoCurrency = algoCurrency;
const algoValidate = algo_validator_1.AlgoValidator.isValidAddress;
exports.algoValidate = algoValidate;
//# sourceMappingURL=algo.js.map