"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mecValidate = exports.mecCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const mecCurrency = {
    name: 'MegaCoin',
    symbol: 'mec',
    addressTypes: {
        prod: [
            '32',
            '05',
        ],
        testnet: [
            '6f',
            'c4',
        ],
    },
};
exports.mecCurrency = mecCurrency;
const mecValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, mecCurrency, opts);
exports.mecValidate = mecValidate;
//# sourceMappingURL=mec.js.map