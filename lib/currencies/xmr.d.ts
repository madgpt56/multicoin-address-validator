import { ValidatorParams } from '../validators/monero_validator';
declare const xmrCurrency: {
    readonly name: "Monero";
    readonly symbol: "xmr";
    readonly addressTypes: {
        readonly prod: readonly ["18", "42"];
        readonly testnet: readonly ["53", "63"];
        readonly stagenet: readonly ["24"];
    };
    readonly iAddressTypes: {
        readonly prod: readonly ["19"];
        readonly testnet: readonly ["54"];
        readonly stagenet: readonly ["25"];
    };
};
declare const xmrValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { xmrCurrency, xmrValidate, };
