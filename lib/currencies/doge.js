"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dogeValidate = exports.dogeCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const dogeCurrency = {
    name: 'DogeCoin',
    symbol: 'doge',
    addressTypes: {
        prod: [
            '1e',
            '16',
        ],
        testnet: [
            '71',
            'c4',
        ],
    },
};
exports.dogeCurrency = dogeCurrency;
const dogeValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, dogeCurrency, opts);
exports.dogeValidate = dogeValidate;
//# sourceMappingURL=doge.js.map