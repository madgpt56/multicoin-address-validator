"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.zecValidate = exports.zecCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const zecCurrency = {
    name: 'ZCash',
    symbol: 'zec',
    expectedLength: 26,
    addressTypes: {
        prod: [
            '1cb8',
            '1cbd',
        ],
        testnet: [
            '1d25',
            '1cba',
        ],
    },
};
exports.zecCurrency = zecCurrency;
const zecValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, zecCurrency, opts);
exports.zecValidate = zecValidate;
//# sourceMappingURL=zec.js.map