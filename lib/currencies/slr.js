"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.slrValidate = exports.slrCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const slrCurrency = {
    name: 'SolarCoin',
    symbol: 'slr',
    addressTypes: {
        prod: [
            '12',
            '05',
        ],
        testnet: [],
    },
};
exports.slrCurrency = slrCurrency;
const slrValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, slrCurrency, opts);
exports.slrValidate = slrValidate;
//# sourceMappingURL=slr.js.map