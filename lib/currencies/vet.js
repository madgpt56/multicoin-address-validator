"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.vetValidate = exports.vetCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const vetCurrency = {
    name: 'VeChain',
    symbol: 'vet',
};
exports.vetCurrency = vetCurrency;
const vetValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.vetValidate = vetValidate;
//# sourceMappingURL=vet.js.map