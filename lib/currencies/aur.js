"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.aurValidate = exports.aurCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const aurCurrency = {
    name: 'AuroraCoin',
    symbol: 'aur',
    addressTypes: {
        prod: [
            '17',
            '05',
        ],
        testnet: [
            '6f',
            'c4',
        ],
    },
};
exports.aurCurrency = aurCurrency;
const aurValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, aurCurrency, opts);
exports.aurValidate = aurValidate;
//# sourceMappingURL=aur.js.map