import { ValidatorParams } from '../validators/bitcoin_validator';
declare const neoCurrency: {
    readonly name: "Neo";
    readonly symbol: "neo";
    readonly addressTypes: {
        readonly prod: readonly ["17"];
        readonly testnet: readonly [];
    };
};
declare const neoValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { neoCurrency, neoValidate, };
