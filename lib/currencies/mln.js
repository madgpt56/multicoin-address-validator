"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mlnValidate = exports.mlnCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const mlnCurrency = {
    name: 'Melon',
    symbol: 'mln',
};
exports.mlnCurrency = mlnCurrency;
const mlnValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.mlnValidate = mlnValidate;
//# sourceMappingURL=mln.js.map