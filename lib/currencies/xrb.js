"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.xrbValidate = exports.xrbCurrency = void 0;
const nano_validator_1 = require("../validators/nano_validator");
const xrbCurrency = {
    name: 'RaiBlocks',
    symbol: 'xrb',
};
exports.xrbCurrency = xrbCurrency;
const xrbValidate = nano_validator_1.NANOValidator.isValidAddress;
exports.xrbValidate = xrbValidate;
//# sourceMappingURL=xrb.js.map