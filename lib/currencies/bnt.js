"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.bntValidate = exports.bntCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const bntCurrency = {
    name: 'Bancor',
    symbol: 'bnt',
};
exports.bntCurrency = bntCurrency;
const bntValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.bntValidate = bntValidate;
//# sourceMappingURL=bnt.js.map