declare const oceanCurrency: {
    readonly name: "Ocean Protocol";
    readonly symbol: "ocean";
};
declare const oceanValidate: (address: string) => boolean;
export { oceanCurrency, oceanValidate, };
