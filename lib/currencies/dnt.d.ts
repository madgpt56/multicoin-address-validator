declare const dntCurrency: {
    readonly name: "District0x";
    readonly symbol: "dnt";
};
declare const dntValidate: (address: string) => boolean;
export { dntCurrency, dntValidate, };
