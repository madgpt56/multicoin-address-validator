import { ValidatorParams } from '../validators/bitcoin_validator';
declare const btcCurrency: {
    readonly name: "Bitcoin";
    readonly symbol: "btc";
    readonly addressTypes: {
        readonly prod: readonly ["00", "05"];
        readonly testnet: readonly ["6f", "c4", "3c", "26"];
    };
    readonly bech32Hrp: {
        readonly prod: readonly ["bc"];
        readonly testnet: readonly ["tb"];
    };
};
declare const btcValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { btcCurrency, btcValidate, };
