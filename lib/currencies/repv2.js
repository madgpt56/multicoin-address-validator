"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.repv2Validate = exports.repv2Currency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const repv2Currency = {
    name: 'AugurV2',
    symbol: 'repv2',
};
exports.repv2Currency = repv2Currency;
const repv2Validate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.repv2Validate = repv2Validate;
//# sourceMappingURL=repv2.js.map