declare const compCurrency: {
    readonly name: "Compound";
    readonly symbol: "comp";
};
declare const compValidate: (address: string) => boolean;
export { compCurrency, compValidate, };
