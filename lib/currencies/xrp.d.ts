declare const xrpCurrency: {
    readonly name: "Ripple";
    readonly symbol: "xrp";
};
declare const xrpValidate: (address: string) => boolean;
export { xrpCurrency, xrpValidate, };
