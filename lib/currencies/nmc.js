"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.nmcValidate = exports.nmcCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const nmcCurrency = {
    name: 'NameCoin',
    symbol: 'nmc',
    addressTypes: {
        prod: [
            '34',
        ],
        testnet: [],
    },
};
exports.nmcCurrency = nmcCurrency;
const nmcValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, nmcCurrency, opts);
exports.nmcValidate = nmcValidate;
//# sourceMappingURL=nmc.js.map