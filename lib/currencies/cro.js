"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.croValidate = exports.croCurrency = void 0;
const bip173_validator_1 = require("../validators/bip173_validator");
const croCurrency = {
    name: 'Crypto.com Coin',
    symbol: 'cro',
    bech32Hrp: { prod: ['cro'], testnet: ['tcro'] },
};
exports.croCurrency = croCurrency;
const croValidate = (address, opts) => bip173_validator_1.BIP173Validator.isValidAddress(address, croCurrency, opts);
exports.croValidate = croValidate;
//# sourceMappingURL=cro.js.map