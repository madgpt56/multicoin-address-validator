declare const cvcCurrency: {
    readonly name: "Civic";
    readonly symbol: "cvc";
};
declare const cvcValidate: (address: string) => boolean;
export { cvcCurrency, cvcValidate, };
