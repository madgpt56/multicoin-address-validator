"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.neoValidate = exports.neoCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const neoCurrency = {
    name: 'Neo',
    symbol: 'neo',
    addressTypes: {
        prod: [
            '17',
        ],
        testnet: [],
    },
};
exports.neoCurrency = neoCurrency;
const neoValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, neoCurrency, opts);
exports.neoValidate = neoValidate;
//# sourceMappingURL=neo.js.map