"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.glmValidate = exports.glmCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const glmCurrency = {
    name: 'Golem',
    symbol: 'glm',
};
exports.glmCurrency = glmCurrency;
const glmValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.glmValidate = glmValidate;
//# sourceMappingURL=glm.js.map