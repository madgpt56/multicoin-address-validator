"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.temcoValidate = exports.temcoCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const temcoCurrency = {
    name: 'TEMCO',
    symbol: 'temco',
};
exports.temcoCurrency = temcoCurrency;
const temcoValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.temcoValidate = temcoValidate;
//# sourceMappingURL=temco.js.map