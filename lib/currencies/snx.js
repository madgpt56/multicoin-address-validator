"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.snxValidate = exports.snxCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const snxCurrency = {
    name: 'Synthetix Network',
    symbol: 'snx',
};
exports.snxCurrency = snxCurrency;
const snxValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.snxValidate = snxValidate;
//# sourceMappingURL=snx.js.map