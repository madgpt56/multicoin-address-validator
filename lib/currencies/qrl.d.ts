declare const qrlCurrency: {
    readonly name: "Quantum Resistant Ledger";
    readonly symbol: "qrl";
};
declare const qrlValidate: (address: string) => boolean;
export { qrlCurrency, qrlValidate, };
