declare const lskCurrency: {
    readonly name: "Lisk";
    readonly symbol: "lsk";
};
declare const lskValidate: (address: string) => boolean;
export { lskCurrency, lskValidate, };
