import { ValidatorParams } from '../validators/bitcoin_validator';
declare const grlcCurrency: {
    readonly name: "GarliCoin";
    readonly symbol: "grlc";
    readonly addressTypes: {
        readonly prod: readonly ["26", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
};
declare const grlcValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { grlcCurrency, grlcValidate, };
