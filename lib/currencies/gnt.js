"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.gntValidate = exports.gntCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const gntCurrency = {
    name: 'Golem (GNT)',
    symbol: 'gnt',
};
exports.gntCurrency = gntCurrency;
const gntValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.gntValidate = gntValidate;
//# sourceMappingURL=gnt.js.map