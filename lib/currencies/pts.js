"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ptsValidate = exports.ptsCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const ptsCurrency = {
    name: 'ProtoShares',
    symbol: 'pts',
    addressTypes: {
        prod: [
            '38',
            '05',
        ],
        testnet: [
            '6f',
            'c4',
        ],
    },
};
exports.ptsCurrency = ptsCurrency;
const ptsValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, ptsCurrency, opts);
exports.ptsValidate = ptsValidate;
//# sourceMappingURL=pts.js.map