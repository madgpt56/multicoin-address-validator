"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.enjValidate = exports.enjCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const enjCurrency = {
    name: 'Enjin Coin',
    symbol: 'enj',
};
exports.enjCurrency = enjCurrency;
const enjValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.enjValidate = enjValidate;
//# sourceMappingURL=enj.js.map