"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.fctValidate = exports.fctCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const fctCurrency = {
    name: 'FirmaChain',
    symbol: 'fct',
};
exports.fctCurrency = fctCurrency;
const fctValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.fctValidate = fctValidate;
//# sourceMappingURL=fct.js.map