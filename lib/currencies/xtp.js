"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.xtpValidate = exports.xtpCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const xtpCurrency = {
    name: 'Tap',
    symbol: 'xtp',
};
exports.xtpCurrency = xtpCurrency;
const xtpValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.xtpValidate = xtpValidate;
//# sourceMappingURL=xtp.js.map