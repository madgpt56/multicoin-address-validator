"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.gupValidate = exports.gupCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const gupCurrency = {
    name: 'Matchpool',
    symbol: 'gup',
};
exports.gupCurrency = gupCurrency;
const gupValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.gupValidate = gupValidate;
//# sourceMappingURL=gup.js.map