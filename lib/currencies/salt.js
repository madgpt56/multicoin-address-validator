"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.saltValidate = exports.saltCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const saltCurrency = {
    name: 'Salt',
    symbol: 'salt',
};
exports.saltCurrency = saltCurrency;
const saltValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.saltValidate = saltValidate;
//# sourceMappingURL=salt.js.map