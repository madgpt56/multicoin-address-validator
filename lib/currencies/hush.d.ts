import { ValidatorParams } from '../validators/bitcoin_validator';
declare const hushCurrency: {
    readonly name: "Hush";
    readonly symbol: "hush";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1cb8", "1cbd"];
        readonly testnet: readonly ["1d25", "1cba"];
    };
};
declare const hushValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { hushCurrency, hushValidate, };
