"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.qrlValidate = exports.qrlCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const qrlCurrency = {
    name: 'Quantum Resistant Ledger',
    symbol: 'qrl',
};
exports.qrlCurrency = qrlCurrency;
const qrlValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.qrlValidate = qrlValidate;
//# sourceMappingURL=qrl.js.map