import { ValidatorParams } from '../validators/bitcoin_validator';
declare const gasCurrency: {
    readonly name: "NeoGas";
    readonly symbol: "gas";
    readonly addressTypes: {
        readonly prod: readonly ["17"];
        readonly testnet: readonly [];
    };
};
declare const gasValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { gasCurrency, gasValidate, };
