declare const mlnCurrency: {
    readonly name: "Melon";
    readonly symbol: "mln";
};
declare const mlnValidate: (address: string) => boolean;
export { mlnCurrency, mlnValidate, };
