declare const servCurrency: {
    readonly name: "Serve";
    readonly symbol: "serv";
};
declare const servValidate: (address: string) => boolean;
export { servCurrency, servValidate, };
