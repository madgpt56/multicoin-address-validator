declare const bntCurrency: {
    readonly name: "Bancor";
    readonly symbol: "bnt";
};
declare const bntValidate: (address: string) => boolean;
export { bntCurrency, bntValidate, };
