declare const batCurrency: {
    readonly name: "Basic Attention Token";
    readonly symbol: "bat";
};
declare const batValidate: (address: string) => boolean;
export { batCurrency, batValidate, };
