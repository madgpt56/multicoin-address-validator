"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.xscValidate = exports.xscCurrency = void 0;
const siacoin_validator_1 = require("../validators/siacoin_validator");
const xscCurrency = {
    name: 'HyperSpace',
    symbol: 'xsc',
};
exports.xscCurrency = xscCurrency;
const xscValidate = siacoin_validator_1.SCValidator.isValidAddress;
exports.xscValidate = xscValidate;
//# sourceMappingURL=xsc.js.map