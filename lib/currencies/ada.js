"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.adaValidate = exports.adaCurrency = void 0;
const ada_validator_1 = require("../validators/ada_validator");
const adaCurrency = {
    name: 'Cardano',
    symbol: 'ada',
    bech32Hrp: { prod: ['addr'], testnet: ['addr'] },
};
exports.adaCurrency = adaCurrency;
const adaValidate = (address, opts) => ada_validator_1.ADAValidator.isValidAddress(address, adaCurrency, opts);
exports.adaValidate = adaValidate;
//# sourceMappingURL=ada.js.map