import { ValidatorParams } from '../validators/bitcoin_validator';
declare const monaCurrency: {
    readonly name: "MonaCoin";
    readonly symbol: "mona";
    readonly addressTypes: {
        readonly prod: readonly ["32", "37"];
        readonly testnet: readonly [];
    };
};
declare const monaValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { monaCurrency, monaValidate, };
