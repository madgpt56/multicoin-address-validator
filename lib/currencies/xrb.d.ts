declare const xrbCurrency: {
    readonly name: "RaiBlocks";
    readonly symbol: "xrb";
};
declare const xrbValidate: (address: string) => boolean;
export { xrbCurrency, xrbValidate, };
