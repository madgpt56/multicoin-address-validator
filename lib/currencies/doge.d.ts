import { ValidatorParams } from '../validators/bitcoin_validator';
declare const dogeCurrency: {
    readonly name: "DogeCoin";
    readonly symbol: "doge";
    readonly addressTypes: {
        readonly prod: readonly ["1e", "16"];
        readonly testnet: readonly ["71", "c4"];
    };
};
declare const dogeValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { dogeCurrency, dogeValidate, };
