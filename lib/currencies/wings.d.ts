declare const wingsCurrency: {
    readonly name: "Wings";
    readonly symbol: "wings";
};
declare const wingsValidate: (address: string) => boolean;
export { wingsCurrency, wingsValidate, };
