"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.oceanValidate = exports.oceanCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const oceanCurrency = {
    name: 'Ocean Protocol',
    symbol: 'ocean',
};
exports.oceanCurrency = oceanCurrency;
const oceanValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.oceanValidate = oceanValidate;
//# sourceMappingURL=ocean.js.map