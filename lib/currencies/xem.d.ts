declare const xemCurrency: {
    readonly name: "Nem";
    readonly symbol: "xem";
};
declare const xemValidate: (_address: string) => boolean;
export { xemCurrency, xemValidate, };
