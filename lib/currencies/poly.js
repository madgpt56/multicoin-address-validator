"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.polyValidate = exports.polyCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const polyCurrency = {
    name: 'Polymath',
    symbol: 'poly',
};
exports.polyCurrency = polyCurrency;
const polyValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.polyValidate = polyValidate;
//# sourceMappingURL=poly.js.map