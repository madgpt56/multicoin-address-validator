import { ValidatorParams } from '../validators/bitcoin_validator';
declare const pivxCurrency: {
    readonly name: "PIVX";
    readonly symbol: "pivx";
    readonly addressTypes: {
        readonly prod: readonly ["1e", "0d"];
        readonly testnet: readonly [];
    };
};
declare const pivxValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { pivxCurrency, pivxValidate, };
