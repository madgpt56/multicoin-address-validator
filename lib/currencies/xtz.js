"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.xtzValidate = exports.xtzCurrency = void 0;
const tezos_validator_1 = require("../validators/tezos_validator");
const xtzCurrency = {
    name: 'Tezos',
    symbol: 'xtz',
};
exports.xtzCurrency = xtzCurrency;
const xtzValidate = tezos_validator_1.XTZValidator.isValidAddress;
exports.xtzValidate = xtzValidate;
//# sourceMappingURL=xtz.js.map