"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.monaValidate = exports.monaCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const monaCurrency = {
    name: 'MonaCoin',
    symbol: 'mona',
    addressTypes: {
        prod: [
            '32',
            '37',
        ],
        testnet: [],
    },
};
exports.monaCurrency = monaCurrency;
const monaValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, monaCurrency, opts);
exports.monaValidate = monaValidate;
//# sourceMappingURL=mona.js.map