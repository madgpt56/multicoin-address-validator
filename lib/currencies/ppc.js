"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ppcValidate = exports.ppcCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const ppcCurrency = {
    name: 'PeerCoin',
    symbol: 'ppc',
    addressTypes: {
        prod: [
            '37',
            '75',
        ],
        testnet: [
            '6f',
            'c4',
        ],
    },
};
exports.ppcCurrency = ppcCurrency;
const ppcValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, ppcCurrency, opts);
exports.ppcValidate = ppcValidate;
//# sourceMappingURL=ppc.js.map