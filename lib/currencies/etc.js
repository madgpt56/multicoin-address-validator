"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.etcValidate = exports.etcCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const etcCurrency = {
    name: 'EthereumClassic',
    symbol: 'etc',
};
exports.etcCurrency = etcCurrency;
const etcValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.etcValidate = etcValidate;
//# sourceMappingURL=etc.js.map