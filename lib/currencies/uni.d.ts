declare const uniCurrency: {
    readonly name: "Uniswap Coin";
    readonly symbol: "uni";
};
declare const uniValidate: (address: string) => boolean;
export { uniCurrency, uniValidate, };
