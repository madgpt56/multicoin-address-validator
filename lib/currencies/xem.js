"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.xemValidate = exports.xemCurrency = void 0;
const nem_validator_1 = require("../validators/nem_validator");
const xemCurrency = {
    name: 'Nem',
    symbol: 'xem',
};
exports.xemCurrency = xemCurrency;
const xemValidate = nem_validator_1.NEMValidator.isValidAddress;
exports.xemValidate = xemValidate;
//# sourceMappingURL=xem.js.map