"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dashValidate = exports.dashCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const dashCurrency = {
    name: 'Dash',
    symbol: 'dash',
    addressTypes: {
        prod: [
            '4c',
            '10',
        ],
        testnet: [
            '8c',
            '13',
        ],
    },
};
exports.dashCurrency = dashCurrency;
const dashValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, dashCurrency, opts);
exports.dashValidate = dashValidate;
//# sourceMappingURL=dash.js.map