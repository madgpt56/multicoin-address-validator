declare const gameCurrency: {
    readonly name: "GameCredits";
    readonly symbol: "game";
    readonly addressTypes: {
        readonly prod: readonly ["26", "05"];
        readonly testnet: readonly [];
    };
};
declare const gameValidate: (address: string) => boolean;
export { gameCurrency, gameValidate, };
