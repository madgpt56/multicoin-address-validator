"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.zclValidate = exports.zclCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const zclCurrency = {
    name: 'ZClassic',
    symbol: 'zcl',
    expectedLength: 26,
    addressTypes: {
        prod: [
            '1cb8',
            '1cbd',
        ],
        testnet: [
            '1d25',
            '1cba',
        ],
    },
};
exports.zclCurrency = zclCurrency;
const zclValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, zclCurrency, opts);
exports.zclValidate = zclValidate;
//# sourceMappingURL=zcl.js.map