"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.frcValidate = exports.frcCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const frcCurrency = {
    name: 'FreiCoin',
    symbol: 'frc',
    addressTypes: {
        prod: [
            '00',
            '05',
        ],
        testnet: [
            '6f',
            'c4',
        ],
    },
};
exports.frcCurrency = frcCurrency;
const frcValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, frcCurrency, opts);
exports.frcValidate = frcValidate;
//# sourceMappingURL=frc.js.map