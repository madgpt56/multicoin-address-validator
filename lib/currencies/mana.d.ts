declare const manaCurrency: {
    readonly name: "Decentraland";
    readonly symbol: "mana";
};
declare const manaValidate: (address: string) => boolean;
export { manaCurrency, manaValidate, };
