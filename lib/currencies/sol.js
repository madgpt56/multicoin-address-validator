"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.solValidate = exports.solCurrency = void 0;
const base58_validator_1 = require("../validators/base58_validator");
const solCurrency = {
    name: 'Solana',
    symbol: 'sol',
    maxLength: 44,
    minLength: 43,
};
exports.solCurrency = solCurrency;
const solValidate = (address) => base58_validator_1.Base58Validator.isValidAddress(address, solCurrency);
exports.solValidate = solValidate;
//# sourceMappingURL=sol.js.map