"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.tusdValidate = exports.tusdCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const tusdCurrency = {
    name: 'TrueUSD',
    symbol: 'tusd',
};
exports.tusdCurrency = tusdCurrency;
const tusdValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.tusdValidate = tusdValidate;
//# sourceMappingURL=tusd.js.map