declare const zrxCurrency: {
    readonly name: "0x";
    readonly symbol: "zrx";
};
declare const zrxValidate: (address: string) => boolean;
export { zrxCurrency, zrxValidate, };
