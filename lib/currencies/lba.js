"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.lbaValidate = exports.lbaCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const lbaCurrency = {
    name: 'Cred',
    symbol: 'lba',
};
exports.lbaCurrency = lbaCurrency;
const lbaValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.lbaValidate = lbaValidate;
//# sourceMappingURL=lba.js.map