"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.hushValidate = exports.hushCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const hushCurrency = {
    name: 'Hush',
    symbol: 'hush',
    expectedLength: 26,
    addressTypes: {
        prod: [
            '1cb8',
            '1cbd',
        ],
        testnet: [
            '1d25',
            '1cba',
        ],
    },
};
exports.hushCurrency = hushCurrency;
const hushValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, hushCurrency, opts);
exports.hushValidate = hushValidate;
//# sourceMappingURL=hush.js.map