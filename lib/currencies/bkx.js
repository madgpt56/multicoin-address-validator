"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.bkxValidate = exports.bkxCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const bkxCurrency = {
    name: 'Bankex',
    symbol: 'bkx',
};
exports.bkxCurrency = bkxCurrency;
const bkxValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.bkxValidate = bkxValidate;
//# sourceMappingURL=bkx.js.map