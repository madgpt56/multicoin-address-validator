declare const fctCurrency: {
    readonly name: "FirmaChain";
    readonly symbol: "fct";
};
declare const fctValidate: (address: string) => boolean;
export { fctCurrency, fctValidate, };
