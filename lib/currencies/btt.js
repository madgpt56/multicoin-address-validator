"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.bttValidate = exports.bttCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const bttCurrency = {
    name: 'BlockTrade',
    symbol: 'btt',
};
exports.bttCurrency = bttCurrency;
const bttValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.bttValidate = bttValidate;
//# sourceMappingURL=btt.js.map