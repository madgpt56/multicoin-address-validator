import { ValidatorParams } from '../validators/bitcoin_validator';
declare const qtumCurrency: {
    readonly name: "Qtum";
    readonly symbol: "qtum";
    readonly addressTypes: {
        readonly prod: readonly ["3a", "32"];
        readonly testnet: readonly ["78", "6e"];
    };
};
declare const qtumValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { qtumCurrency, qtumValidate, };
