"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.btuValidate = exports.btuCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const btuCurrency = {
    name: 'BTU Protocol',
    symbol: 'btu',
};
exports.btuCurrency = btuCurrency;
const btuValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.btuValidate = btuValidate;
//# sourceMappingURL=btu.js.map