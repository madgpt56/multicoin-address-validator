"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ocnValidate = exports.ocnCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const ocnCurrency = {
    name: 'Odyssey',
    symbol: 'ocn',
};
exports.ocnCurrency = ocnCurrency;
const ocnValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.ocnValidate = ocnValidate;
//# sourceMappingURL=ocn.js.map