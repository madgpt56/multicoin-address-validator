"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.btcpValidate = exports.btcpCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const btcpCurrency = {
    name: 'BitcoinPrivate',
    symbol: 'btcp',
    expectedLength: 26,
    addressTypes: {
        prod: [
            '1325',
            '13af',
        ],
        testnet: [
            '1957',
            '19e0',
        ],
    },
};
exports.btcpCurrency = btcpCurrency;
const btcpValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, btcpCurrency, opts);
exports.btcpValidate = btcpValidate;
//# sourceMappingURL=btcp.js.map