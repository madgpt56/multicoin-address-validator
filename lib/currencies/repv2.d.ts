declare const repv2Currency: {
    readonly name: "AugurV2";
    readonly symbol: "repv2";
};
declare const repv2Validate: (address: string) => boolean;
export { repv2Currency, repv2Validate, };
