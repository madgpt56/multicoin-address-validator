"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.wavesValidate = exports.wavesCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const wavesCurrency = {
    name: 'Waves',
    symbol: 'waves',
    addressTypes: { prod: ['0157'], testnet: ['0154'] },
    expectedLength: 26,
    hashFunction: 'blake256keccak256',
    regex: /^[a-zA-Z0-9]{35}$/,
};
exports.wavesCurrency = wavesCurrency;
const wavesValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, wavesCurrency, opts);
exports.wavesValidate = wavesValidate;
//# sourceMappingURL=waves.js.map