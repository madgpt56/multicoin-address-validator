declare const cusdCurrency: {
    readonly name: "CUSD";
    readonly symbol: "cusd";
};
declare const cusdValidate: (address: string) => boolean;
export { cusdCurrency, cusdValidate, };
