"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.bsvValidate = exports.bsvCurrency = void 0;
const bch_validator_1 = require("../validators/bch_validator");
const bsvCurrency = {
    name: 'Bitcoin SV',
    symbol: 'bsv',
    regexp: '^[qQ]{1}[0-9a-zA-Z]{41}$',
    addressTypes: { prod: ['00', '05'], testnet: ['6f', 'c4'] },
};
exports.bsvCurrency = bsvCurrency;
const bsvValidate = (address, opts) => bch_validator_1.BCHValidator.isValidAddress(address, bsvCurrency, opts);
exports.bsvValidate = bsvValidate;
//# sourceMappingURL=bsv.js.map