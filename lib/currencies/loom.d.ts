declare const loomCurrency: {
    readonly name: "Loom Network";
    readonly symbol: "loom";
};
declare const loomValidate: (address: string) => boolean;
export { loomCurrency, loomValidate, };
