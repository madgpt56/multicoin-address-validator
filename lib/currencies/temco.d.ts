declare const temcoCurrency: {
    readonly name: "TEMCO";
    readonly symbol: "temco";
};
declare const temcoValidate: (address: string) => boolean;
export { temcoCurrency, temcoValidate, };
