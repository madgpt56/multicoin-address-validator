"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.compValidate = exports.compCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const compCurrency = {
    name: 'Compound',
    symbol: 'comp',
};
exports.compCurrency = compCurrency;
const compValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.compValidate = compValidate;
//# sourceMappingURL=comp.js.map