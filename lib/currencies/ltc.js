"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ltcValidate = exports.ltcCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const ltcCurrency = {
    name: 'LiteCoin',
    symbol: 'ltc',
    addressTypes: {
        prod: [
            '30',
            '05',
            '32',
        ],
        testnet: [
            '6f',
            'c4',
            '3a',
        ],
    },
    bech32Hrp: {
        prod: [
            'ltc',
        ],
        testnet: [
            'tltc',
        ],
    },
};
exports.ltcCurrency = ltcCurrency;
const ltcValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, ltcCurrency, opts);
exports.ltcValidate = ltcValidate;
//# sourceMappingURL=ltc.js.map