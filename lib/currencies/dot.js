"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dotValidate = exports.dotCurrency = void 0;
const dot_validator_1 = require("../validators/dot_validator");
const dotCurrency = {
    name: 'Polkadot',
    symbol: 'dot',
};
exports.dotCurrency = dotCurrency;
const dotValidate = dot_validator_1.DotValidator.isValidAddress;
exports.dotValidate = dotValidate;
//# sourceMappingURL=dot.js.map