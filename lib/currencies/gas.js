"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.gasValidate = exports.gasCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const gasCurrency = {
    name: 'NeoGas',
    symbol: 'gas',
    addressTypes: {
        prod: [
            '17',
        ],
        testnet: [],
    },
};
exports.gasCurrency = gasCurrency;
const gasValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, gasCurrency, opts);
exports.gasValidate = gasValidate;
//# sourceMappingURL=gas.js.map