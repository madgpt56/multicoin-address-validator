import { ValidatorParams } from '../validators/bitcoin_validator';
declare const btgCurrency: {
    readonly name: "BitcoinGold";
    readonly symbol: "btg";
    readonly addressTypes: {
        readonly prod: readonly ["26", "17"];
        readonly testnet: readonly ["6f", "c4"];
    };
};
declare const btgValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { btgCurrency, btgValidate, };
