"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.vtcValidate = exports.vtcCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const vtcCurrency = {
    name: 'VertCoin',
    symbol: 'vtc',
    addressTypes: {
        prod: [
            '0x',
            '47',
            '71',
            '05',
        ],
        testnet: [
            '6f',
            'c4',
        ],
    },
};
exports.vtcCurrency = vtcCurrency;
const vtcValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, vtcCurrency, opts);
exports.vtcValidate = vtcValidate;
//# sourceMappingURL=vtc.js.map