import { ValidatorParams } from '../validators/bitcoin_validator';
declare const lbcCurrency: {
    readonly name: "LBRY Credits";
    readonly symbol: "lbc";
    readonly addressTypes: {
        readonly prod: readonly ["55"];
        readonly testnet: readonly [];
    };
};
declare const lbcValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { lbcCurrency, lbcValidate, };
