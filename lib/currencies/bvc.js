"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.bvcValidate = exports.bvcCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const bvcCurrency = {
    name: 'BeaverCoin',
    symbol: 'bvc',
    addressTypes: {
        prod: [
            '19',
            '05',
        ],
        testnet: [
            '6f',
            'c4',
        ],
    },
};
exports.bvcCurrency = bvcCurrency;
const bvcValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, bvcCurrency, opts);
exports.bvcValidate = bvcValidate;
//# sourceMappingURL=bvc.js.map