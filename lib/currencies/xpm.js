"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.xpmValidate = exports.xpmCurrency = void 0;
const bitcoin_validator_1 = require("../validators/bitcoin_validator");
const xpmCurrency = {
    name: 'PrimeCoin',
    symbol: 'xpm',
    addressTypes: {
        prod: [
            '17',
            '53',
        ],
        testnet: [
            '6f',
            'c4',
        ],
    },
};
exports.xpmCurrency = xpmCurrency;
const xpmValidate = (address, opts) => bitcoin_validator_1.BTCValidator.isValidAddress(address, xpmCurrency, opts);
exports.xpmValidate = xpmValidate;
//# sourceMappingURL=xpm.js.map