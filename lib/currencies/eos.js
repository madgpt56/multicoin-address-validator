"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.eosValidate = exports.eosCurrency = void 0;
const eos_validator_1 = require("../validators/eos_validator");
const eosCurrency = {
    name: 'EOS',
    symbol: 'eos',
};
exports.eosCurrency = eosCurrency;
const eosValidate = eos_validator_1.EOSValidator.isValidAddress;
exports.eosValidate = eosValidate;
//# sourceMappingURL=eos.js.map