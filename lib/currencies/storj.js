"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.storjValidate = exports.storjCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const storjCurrency = {
    name: 'Storj',
    symbol: 'storj',
};
exports.storjCurrency = storjCurrency;
const storjValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.storjValidate = storjValidate;
//# sourceMappingURL=storj.js.map