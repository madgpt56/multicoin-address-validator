import { ValidatorParams } from '../validators/bitcoin_validator';
declare const xpmCurrency: {
    readonly name: "PrimeCoin";
    readonly symbol: "xpm";
    readonly addressTypes: {
        readonly prod: readonly ["17", "53"];
        readonly testnet: readonly ["6f", "c4"];
    };
};
declare const xpmValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { xpmCurrency, xpmValidate, };
