"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.wingsValidate = exports.wingsCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const wingsCurrency = {
    name: 'Wings',
    symbol: 'wings',
};
exports.wingsCurrency = wingsCurrency;
const wingsValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.wingsValidate = wingsValidate;
//# sourceMappingURL=wings.js.map