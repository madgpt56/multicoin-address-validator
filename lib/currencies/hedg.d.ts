declare const hedgCurrency: {
    readonly name: "HedgeTrade";
    readonly symbol: "hedg";
};
declare const hedgValidate: (address: string) => boolean;
export { hedgCurrency, hedgValidate, };
