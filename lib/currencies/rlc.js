"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.rlcValidate = exports.rlcCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const rlcCurrency = {
    name: 'iExec RLC',
    symbol: 'rlc',
};
exports.rlcCurrency = rlcCurrency;
const rlcValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.rlcValidate = rlcValidate;
//# sourceMappingURL=rlc.js.map