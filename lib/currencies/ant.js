"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.antValidate = exports.antCurrency = void 0;
const ethereum_validator_1 = require("../validators/ethereum_validator");
const antCurrency = {
    name: 'Aragon',
    symbol: 'ant',
};
exports.antCurrency = antCurrency;
const antValidate = ethereum_validator_1.ETHValidator.isValidAddress;
exports.antValidate = antValidate;
//# sourceMappingURL=ant.js.map