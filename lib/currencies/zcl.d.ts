import { ValidatorParams } from '../validators/bitcoin_validator';
declare const zclCurrency: {
    readonly name: "ZClassic";
    readonly symbol: "zcl";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1cb8", "1cbd"];
        readonly testnet: readonly ["1d25", "1cba"];
    };
};
declare const zclValidate: (address: ValidatorParams[0], opts?: ValidatorParams[2]) => boolean;
export { zclCurrency, zclValidate, };
