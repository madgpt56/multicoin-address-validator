declare const nanoCurrency: {
    readonly name: "Nano";
    readonly symbol: "nano";
};
declare const nanoValidate: (address: string) => boolean;
export { nanoCurrency, nanoValidate, };
