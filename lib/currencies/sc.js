"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.scValidate = exports.scCurrency = void 0;
const siacoin_validator_1 = require("../validators/siacoin_validator");
const scCurrency = {
    name: 'Siacoin',
    symbol: 'sc',
};
exports.scCurrency = scCurrency;
const scValidate = siacoin_validator_1.SCValidator.isValidAddress;
exports.scValidate = scValidate;
//# sourceMappingURL=sc.js.map