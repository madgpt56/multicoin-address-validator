"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getSupportedSymbols = exports.validate = void 0;
const currencies_1 = require("./currencies");
Object.defineProperty(exports, "getSupportedSymbols", { enumerable: true, get: function () { return currencies_1.getSupportedSymbols; } });
function validate(address, currencySymbolOrName, networkTypeOrOpts) {
    if (typeof address !== 'string') {
        return false; // protection against untyped input
    }
    const currency = (0, currencies_1.getCurrency)(currencySymbolOrName);
    if (!currency) {
        throw new Error(`Missing validator for currency: ${currencySymbolOrName}`);
    }
    const opts = typeof networkTypeOrOpts === 'string'
        ? { networkType: networkTypeOrOpts }
        : networkTypeOrOpts;
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return currency.validate(address, opts);
}
exports.validate = validate;
//# sourceMappingURL=index.js.map