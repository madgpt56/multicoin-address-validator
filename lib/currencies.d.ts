declare const currencies: readonly [{
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "Bitcoin";
    readonly symbol: "btc";
    readonly addressTypes: {
        readonly prod: readonly ["00", "05"];
        readonly testnet: readonly ["6f", "c4", "3c", "26"];
    };
    readonly bech32Hrp: {
        readonly prod: readonly ["bc"];
        readonly testnet: readonly ["tb"];
    };
}, {
    readonly validate: (address: string, opts?: import("./types").OptsNetworkTypeOptional<"prod" | "testnet"> | undefined) => boolean;
    readonly name: "BitcoinCash";
    readonly symbol: "bch";
    readonly regexp: "^[qQpP]{1}[0-9a-zA-Z]{41}$";
    readonly addressTypes: {
        readonly prod: readonly ["00", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
}, {
    readonly validate: (address: string, opts?: import("./types").OptsNetworkTypeOptional<"prod" | "testnet"> | undefined) => boolean;
    readonly name: "Bitcoin SV";
    readonly symbol: "bsv";
    readonly regexp: "^[qQ]{1}[0-9a-zA-Z]{41}$";
    readonly addressTypes: {
        readonly prod: readonly ["00", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "LiteCoin";
    readonly symbol: "ltc";
    readonly addressTypes: {
        readonly prod: readonly ["30", "05", "32"];
        readonly testnet: readonly ["6f", "c4", "3a"];
    };
    readonly bech32Hrp: {
        readonly prod: readonly ["ltc"];
        readonly testnet: readonly ["tltc"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "PeerCoin";
    readonly symbol: "ppc";
    readonly addressTypes: {
        readonly prod: readonly ["37", "75"];
        readonly testnet: readonly ["6f", "c4"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "DogeCoin";
    readonly symbol: "doge";
    readonly addressTypes: {
        readonly prod: readonly ["1e", "16"];
        readonly testnet: readonly ["71", "c4"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "BeaverCoin";
    readonly symbol: "bvc";
    readonly addressTypes: {
        readonly prod: readonly ["19", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "FreiCoin";
    readonly symbol: "frc";
    readonly addressTypes: {
        readonly prod: readonly ["00", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "ProtoShares";
    readonly symbol: "pts";
    readonly addressTypes: {
        readonly prod: readonly ["38", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "MegaCoin";
    readonly symbol: "mec";
    readonly addressTypes: {
        readonly prod: readonly ["32", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "PrimeCoin";
    readonly symbol: "xpm";
    readonly addressTypes: {
        readonly prod: readonly ["17", "53"];
        readonly testnet: readonly ["6f", "c4"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "AuroraCoin";
    readonly symbol: "aur";
    readonly addressTypes: {
        readonly prod: readonly ["17", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "NameCoin";
    readonly symbol: "nmc";
    readonly addressTypes: {
        readonly prod: readonly ["34"];
        readonly testnet: readonly [];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "BioCoin";
    readonly symbol: "bio";
    readonly addressTypes: {
        readonly prod: readonly ["19", "14"];
        readonly testnet: readonly ["6f", "c4"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "GarliCoin";
    readonly symbol: "grlc";
    readonly addressTypes: {
        readonly prod: readonly ["26", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "VertCoin";
    readonly symbol: "vtc";
    readonly addressTypes: {
        readonly prod: readonly ["0x", "47", "71", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "BitcoinGold";
    readonly symbol: "btg";
    readonly addressTypes: {
        readonly prod: readonly ["26", "17"];
        readonly testnet: readonly ["6f", "c4"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "Komodo";
    readonly symbol: "kmd";
    readonly addressTypes: {
        readonly prod: readonly ["3c", "55"];
        readonly testnet: readonly ["0", "5"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "BitcoinZ";
    readonly symbol: "btcz";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1cb8", "1cbd"];
        readonly testnet: readonly ["1d25", "1cba"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "BitcoinPrivate";
    readonly symbol: "btcp";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1325", "13af"];
        readonly testnet: readonly ["1957", "19e0"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "Hush";
    readonly symbol: "hush";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1cb8", "1cbd"];
        readonly testnet: readonly ["1d25", "1cba"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "SnowGem";
    readonly symbol: "sng";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1c28", "1c2d"];
        readonly testnet: readonly ["1d25", "1cba"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "ZCash";
    readonly symbol: "zec";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1cb8", "1cbd"];
        readonly testnet: readonly ["1d25", "1cba"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "ZClassic";
    readonly symbol: "zcl";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1cb8", "1cbd"];
        readonly testnet: readonly ["1d25", "1cba"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "ZenCash";
    readonly symbol: "zen";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["2089", "2096"];
        readonly testnet: readonly ["2092", "2098"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "VoteCoin";
    readonly symbol: "vot";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1cb8", "1cbd"];
        readonly testnet: readonly ["1d25", "1cba"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "Decred";
    readonly symbol: "dcr";
    readonly addressTypes: {
        readonly prod: readonly ["073f", "071a"];
        readonly testnet: readonly ["0f21", "0efc"];
    };
    readonly hashFunction: "blake256";
    readonly expectedLength: 26;
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "GameCredits";
    readonly symbol: "game";
    readonly addressTypes: {
        readonly prod: readonly ["26", "05"];
        readonly testnet: readonly [];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "PIVX";
    readonly symbol: "pivx";
    readonly addressTypes: {
        readonly prod: readonly ["1e", "0d"];
        readonly testnet: readonly [];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "SolarCoin";
    readonly symbol: "slr";
    readonly addressTypes: {
        readonly prod: readonly ["12", "05"];
        readonly testnet: readonly [];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "MonaCoin";
    readonly symbol: "mona";
    readonly addressTypes: {
        readonly prod: readonly ["32", "37"];
        readonly testnet: readonly [];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "DigiByte";
    readonly symbol: "dgb";
    readonly addressTypes: {
        readonly prod: readonly ["1e", "3f"];
        readonly testnet: readonly [];
    };
    readonly bech32Hrp: {
        readonly prod: readonly ["dgb", "S"];
        readonly testnet: readonly [];
    };
}, {
    readonly validate: (address: string, opts?: (import("./types").OptsNetworkTypeOptional<"prod" | "testnet"> & {
        chainType?: "erc20" | "omni" | undefined;
    }) | undefined) => boolean;
    readonly name: "Tether";
    readonly symbol: "usdt";
    readonly addressTypes: {
        readonly prod: readonly ["00", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Ripple";
    readonly symbol: "xrp";
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "Dash";
    readonly symbol: "dash";
    readonly addressTypes: {
        readonly prod: readonly ["4c", "10"];
        readonly testnet: readonly ["8c", "13"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "Neo";
    readonly symbol: "neo";
    readonly addressTypes: {
        readonly prod: readonly ["17"];
        readonly testnet: readonly [];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "NeoGas";
    readonly symbol: "gas";
    readonly addressTypes: {
        readonly prod: readonly ["17"];
        readonly testnet: readonly [];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "Qtum";
    readonly symbol: "qtum";
    readonly addressTypes: {
        readonly prod: readonly ["3a", "32"];
        readonly testnet: readonly ["78", "6e"];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "Waves";
    readonly symbol: "waves";
    readonly addressTypes: {
        readonly prod: readonly ["0157"];
        readonly testnet: readonly ["0154"];
    };
    readonly expectedLength: 26;
    readonly hashFunction: "blake256keccak256";
    readonly regex: RegExp;
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Ethereum";
    readonly symbol: "eth";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "EtherZero";
    readonly symbol: "etz";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "EthereumClassic";
    readonly symbol: "etc";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Callisto";
    readonly symbol: "clo";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Bankex";
    readonly symbol: "bkx";
}, {
    readonly validate: (address: string, opts?: import("./types").OptsNetworkTypeOptional<"prod" | "testnet"> | undefined) => boolean;
    readonly name: "Cardano";
    readonly symbol: "ada";
    readonly bech32Hrp: {
        readonly prod: readonly ["addr"];
        readonly testnet: readonly ["addr"];
    };
}, {
    readonly validate: (address: string, opts?: import("./types").OptsNetworkTypeOptional<"prod" | "testnet" | "stagenet" | "both"> | undefined) => boolean;
    readonly name: "Monero";
    readonly symbol: "xmr";
    readonly addressTypes: {
        readonly prod: readonly ["18", "42"];
        readonly testnet: readonly ["53", "63"];
        readonly stagenet: readonly ["24"];
    };
    readonly iAddressTypes: {
        readonly prod: readonly ["19"];
        readonly testnet: readonly ["54"];
        readonly stagenet: readonly ["25"];
    };
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Aragon";
    readonly symbol: "ant";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Basic Attention Token";
    readonly symbol: "bat";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Bancor";
    readonly symbol: "bnt";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Civic";
    readonly symbol: "cvc";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "District0x";
    readonly symbol: "dnt";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Gnosis";
    readonly symbol: "gno";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Golem (GNT)";
    readonly symbol: "gnt";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Golem";
    readonly symbol: "glm";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Matchpool";
    readonly symbol: "gup";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Melon";
    readonly symbol: "mln";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Numeraire";
    readonly symbol: "nmr";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "OmiseGO";
    readonly symbol: "omg";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "TenX";
    readonly symbol: "pay";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Ripio Credit Network";
    readonly symbol: "rcn";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Augur";
    readonly symbol: "rep";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "iExec RLC";
    readonly symbol: "rlc";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Salt";
    readonly symbol: "salt";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Status";
    readonly symbol: "snt";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Storj";
    readonly symbol: "storj";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Swarm City";
    readonly symbol: "swt";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "TrueUSD";
    readonly symbol: "tusd";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Wings";
    readonly symbol: "wings";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "0x";
    readonly symbol: "zrx";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Expanse";
    readonly symbol: "exp";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Viberate";
    readonly symbol: "vib";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Odyssey";
    readonly symbol: "ocn";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Polymath";
    readonly symbol: "poly";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Storm";
    readonly symbol: "storm";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Nano";
    readonly symbol: "nano";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "RaiBlocks";
    readonly symbol: "xrb";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Siacoin";
    readonly symbol: "sc";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "HyperSpace";
    readonly symbol: "xsc";
}, {
    readonly validate: (address: string, opts?: import("./types").OptsNetworkTypeOptional<"prod" | "testnet" | "stagenet" | "both"> | undefined) => boolean;
    readonly name: "loki";
    readonly symbol: "loki";
    readonly addressTypes: {
        readonly prod: readonly ["114", "115", "116"];
        readonly testnet: readonly [];
    };
    readonly iAddressTypes: {
        readonly prod: readonly ["115"];
        readonly testnet: readonly [];
    };
}, {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "LBRY Credits";
    readonly symbol: "lbc";
    readonly addressTypes: {
        readonly prod: readonly ["55"];
        readonly testnet: readonly [];
    };
}, {
    readonly validate: (address: string, opts?: import("./types").OptsNetworkTypeOptional<"prod" | "testnet"> | undefined) => boolean;
    readonly name: "Tron";
    readonly symbol: "trx";
    readonly addressTypes: {
        readonly prod: readonly [65];
        readonly testnet: readonly [160];
    };
}, {
    readonly validate: (_address: string) => boolean;
    readonly name: "Nem";
    readonly symbol: "xem";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Lisk";
    readonly symbol: "lsk";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Stellar";
    readonly symbol: "xlm";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "BTU Protocol";
    readonly symbol: "btu";
}, {
    readonly validate: (address: string, opts?: import("./types").OptsNetworkTypeOptional<"prod" | "testnet"> | undefined) => boolean;
    readonly name: "Crypto.com Coin";
    readonly symbol: "cro";
    readonly bech32Hrp: {
        readonly prod: readonly ["cro"];
        readonly testnet: readonly ["tcro"];
    };
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Multi-collateral DAI";
    readonly symbol: "dai";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Enjin Coin";
    readonly symbol: "enj";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "HedgeTrade";
    readonly symbol: "hedg";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Cred";
    readonly symbol: "lba";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Chainlink";
    readonly symbol: "link";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Loom Network";
    readonly symbol: "loom";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Maker";
    readonly symbol: "mkr";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Metal";
    readonly symbol: "mtl";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Ocean Protocol";
    readonly symbol: "ocean";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Quant";
    readonly symbol: "qnt";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Synthetix Network";
    readonly symbol: "snx";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "SOLVE";
    readonly symbol: "solve";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Spendcoin";
    readonly symbol: "spnd";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "TEMCO";
    readonly symbol: "temco";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "EOS";
    readonly symbol: "eos";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Tezos";
    readonly symbol: "xtz";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "VeChain";
    readonly symbol: "vet";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "StormX";
    readonly symbol: "stmx";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "AugurV2";
    readonly symbol: "repv2";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "FirmaChain";
    readonly symbol: "fct";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "BlockTrade";
    readonly symbol: "btt";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Quantum Resistant Ledger";
    readonly symbol: "qrl";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Serve";
    readonly symbol: "serv";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Tap";
    readonly symbol: "xtp";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Compound";
    readonly symbol: "comp";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Paxos";
    readonly symbol: "pax";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "USD Coin";
    readonly symbol: "usdc";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "CUSD";
    readonly symbol: "cusd";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Algorand";
    readonly symbol: "algo";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Polkadot";
    readonly symbol: "dot";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Uniswap Coin";
    readonly symbol: "uni";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Aave Coin";
    readonly symbol: "aave";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Matic";
    readonly symbol: "matic";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Decentraland";
    readonly symbol: "mana";
}, {
    readonly validate: (address: string) => boolean;
    readonly name: "Solana";
    readonly symbol: "sol";
    readonly maxLength: 44;
    readonly minLength: 43;
}];
declare type Currency = (typeof currencies)[number];
declare type CurrencySymbolReal = Currency['symbol'];
declare type CurrencySymbolAnyRegister = CurrencySymbolReal | Uppercase<CurrencySymbolReal> | Lowercase<CurrencySymbolReal>;
declare type CurrencyNameReal = Currency['name'];
declare type CurrencyNameAnyRegister = CurrencyNameReal | Uppercase<CurrencyNameReal> | Lowercase<CurrencyNameReal>;
declare type ValidateOpts = Parameters<Currency['validate']>[1];
declare function getCurrency(symbolOrName: CurrencySymbolAnyRegister | CurrencyNameAnyRegister): {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "Bitcoin";
    readonly symbol: "btc";
    readonly addressTypes: {
        readonly prod: readonly ["00", "05"];
        readonly testnet: readonly ["6f", "c4", "3c", "26"];
    };
    readonly bech32Hrp: {
        readonly prod: readonly ["bc"];
        readonly testnet: readonly ["tb"];
    };
} | {
    readonly validate: (address: string, opts?: import("./types").OptsNetworkTypeOptional<"prod" | "testnet"> | undefined) => boolean;
    readonly name: "BitcoinCash";
    readonly symbol: "bch";
    readonly regexp: "^[qQpP]{1}[0-9a-zA-Z]{41}$";
    readonly addressTypes: {
        readonly prod: readonly ["00", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
} | {
    readonly validate: (address: string, opts?: import("./types").OptsNetworkTypeOptional<"prod" | "testnet"> | undefined) => boolean;
    readonly name: "Bitcoin SV";
    readonly symbol: "bsv";
    readonly regexp: "^[qQ]{1}[0-9a-zA-Z]{41}$";
    readonly addressTypes: {
        readonly prod: readonly ["00", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "LiteCoin";
    readonly symbol: "ltc";
    readonly addressTypes: {
        readonly prod: readonly ["30", "05", "32"];
        readonly testnet: readonly ["6f", "c4", "3a"];
    };
    readonly bech32Hrp: {
        readonly prod: readonly ["ltc"];
        readonly testnet: readonly ["tltc"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "PeerCoin";
    readonly symbol: "ppc";
    readonly addressTypes: {
        readonly prod: readonly ["37", "75"];
        readonly testnet: readonly ["6f", "c4"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "DogeCoin";
    readonly symbol: "doge";
    readonly addressTypes: {
        readonly prod: readonly ["1e", "16"];
        readonly testnet: readonly ["71", "c4"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "BeaverCoin";
    readonly symbol: "bvc";
    readonly addressTypes: {
        readonly prod: readonly ["19", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "FreiCoin";
    readonly symbol: "frc";
    readonly addressTypes: {
        readonly prod: readonly ["00", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "ProtoShares";
    readonly symbol: "pts";
    readonly addressTypes: {
        readonly prod: readonly ["38", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "MegaCoin";
    readonly symbol: "mec";
    readonly addressTypes: {
        readonly prod: readonly ["32", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "PrimeCoin";
    readonly symbol: "xpm";
    readonly addressTypes: {
        readonly prod: readonly ["17", "53"];
        readonly testnet: readonly ["6f", "c4"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "AuroraCoin";
    readonly symbol: "aur";
    readonly addressTypes: {
        readonly prod: readonly ["17", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "NameCoin";
    readonly symbol: "nmc";
    readonly addressTypes: {
        readonly prod: readonly ["34"];
        readonly testnet: readonly [];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "BioCoin";
    readonly symbol: "bio";
    readonly addressTypes: {
        readonly prod: readonly ["19", "14"];
        readonly testnet: readonly ["6f", "c4"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "GarliCoin";
    readonly symbol: "grlc";
    readonly addressTypes: {
        readonly prod: readonly ["26", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "VertCoin";
    readonly symbol: "vtc";
    readonly addressTypes: {
        readonly prod: readonly ["0x", "47", "71", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "BitcoinGold";
    readonly symbol: "btg";
    readonly addressTypes: {
        readonly prod: readonly ["26", "17"];
        readonly testnet: readonly ["6f", "c4"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "Komodo";
    readonly symbol: "kmd";
    readonly addressTypes: {
        readonly prod: readonly ["3c", "55"];
        readonly testnet: readonly ["0", "5"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "BitcoinZ";
    readonly symbol: "btcz";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1cb8", "1cbd"];
        readonly testnet: readonly ["1d25", "1cba"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "BitcoinPrivate";
    readonly symbol: "btcp";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1325", "13af"];
        readonly testnet: readonly ["1957", "19e0"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "Hush";
    readonly symbol: "hush";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1cb8", "1cbd"];
        readonly testnet: readonly ["1d25", "1cba"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "SnowGem";
    readonly symbol: "sng";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1c28", "1c2d"];
        readonly testnet: readonly ["1d25", "1cba"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "ZCash";
    readonly symbol: "zec";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1cb8", "1cbd"];
        readonly testnet: readonly ["1d25", "1cba"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "ZClassic";
    readonly symbol: "zcl";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1cb8", "1cbd"];
        readonly testnet: readonly ["1d25", "1cba"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "ZenCash";
    readonly symbol: "zen";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["2089", "2096"];
        readonly testnet: readonly ["2092", "2098"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "VoteCoin";
    readonly symbol: "vot";
    readonly expectedLength: 26;
    readonly addressTypes: {
        readonly prod: readonly ["1cb8", "1cbd"];
        readonly testnet: readonly ["1d25", "1cba"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "Decred";
    readonly symbol: "dcr";
    readonly addressTypes: {
        readonly prod: readonly ["073f", "071a"];
        readonly testnet: readonly ["0f21", "0efc"];
    };
    readonly hashFunction: "blake256";
    readonly expectedLength: 26;
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "GameCredits";
    readonly symbol: "game";
    readonly addressTypes: {
        readonly prod: readonly ["26", "05"];
        readonly testnet: readonly [];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "PIVX";
    readonly symbol: "pivx";
    readonly addressTypes: {
        readonly prod: readonly ["1e", "0d"];
        readonly testnet: readonly [];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "SolarCoin";
    readonly symbol: "slr";
    readonly addressTypes: {
        readonly prod: readonly ["12", "05"];
        readonly testnet: readonly [];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "MonaCoin";
    readonly symbol: "mona";
    readonly addressTypes: {
        readonly prod: readonly ["32", "37"];
        readonly testnet: readonly [];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "DigiByte";
    readonly symbol: "dgb";
    readonly addressTypes: {
        readonly prod: readonly ["1e", "3f"];
        readonly testnet: readonly [];
    };
    readonly bech32Hrp: {
        readonly prod: readonly ["dgb", "S"];
        readonly testnet: readonly [];
    };
} | {
    readonly validate: (address: string, opts?: (import("./types").OptsNetworkTypeOptional<"prod" | "testnet"> & {
        chainType?: "erc20" | "omni" | undefined;
    }) | undefined) => boolean;
    readonly name: "Tether";
    readonly symbol: "usdt";
    readonly addressTypes: {
        readonly prod: readonly ["00", "05"];
        readonly testnet: readonly ["6f", "c4"];
    };
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Ripple";
    readonly symbol: "xrp";
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "Dash";
    readonly symbol: "dash";
    readonly addressTypes: {
        readonly prod: readonly ["4c", "10"];
        readonly testnet: readonly ["8c", "13"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "Neo";
    readonly symbol: "neo";
    readonly addressTypes: {
        readonly prod: readonly ["17"];
        readonly testnet: readonly [];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "NeoGas";
    readonly symbol: "gas";
    readonly addressTypes: {
        readonly prod: readonly ["17"];
        readonly testnet: readonly [];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "Qtum";
    readonly symbol: "qtum";
    readonly addressTypes: {
        readonly prod: readonly ["3a", "32"];
        readonly testnet: readonly ["78", "6e"];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "Waves";
    readonly symbol: "waves";
    readonly addressTypes: {
        readonly prod: readonly ["0157"];
        readonly testnet: readonly ["0154"];
    };
    readonly expectedLength: 26;
    readonly hashFunction: "blake256keccak256";
    readonly regex: RegExp;
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Ethereum";
    readonly symbol: "eth";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "EtherZero";
    readonly symbol: "etz";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "EthereumClassic";
    readonly symbol: "etc";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Callisto";
    readonly symbol: "clo";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Bankex";
    readonly symbol: "bkx";
} | {
    readonly validate: (address: string, opts?: import("./types").OptsNetworkTypeOptional<"prod" | "testnet"> | undefined) => boolean;
    readonly name: "Cardano";
    readonly symbol: "ada";
    readonly bech32Hrp: {
        readonly prod: readonly ["addr"];
        readonly testnet: readonly ["addr"];
    };
} | {
    readonly validate: (address: string, opts?: import("./types").OptsNetworkTypeOptional<"prod" | "testnet" | "stagenet" | "both"> | undefined) => boolean;
    readonly name: "Monero";
    readonly symbol: "xmr";
    readonly addressTypes: {
        readonly prod: readonly ["18", "42"];
        readonly testnet: readonly ["53", "63"];
        readonly stagenet: readonly ["24"];
    };
    readonly iAddressTypes: {
        readonly prod: readonly ["19"];
        readonly testnet: readonly ["54"];
        readonly stagenet: readonly ["25"];
    };
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Aragon";
    readonly symbol: "ant";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Basic Attention Token";
    readonly symbol: "bat";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Bancor";
    readonly symbol: "bnt";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Civic";
    readonly symbol: "cvc";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "District0x";
    readonly symbol: "dnt";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Gnosis";
    readonly symbol: "gno";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Golem (GNT)";
    readonly symbol: "gnt";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Golem";
    readonly symbol: "glm";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Matchpool";
    readonly symbol: "gup";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Melon";
    readonly symbol: "mln";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Numeraire";
    readonly symbol: "nmr";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "OmiseGO";
    readonly symbol: "omg";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "TenX";
    readonly symbol: "pay";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Ripio Credit Network";
    readonly symbol: "rcn";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Augur";
    readonly symbol: "rep";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "iExec RLC";
    readonly symbol: "rlc";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Salt";
    readonly symbol: "salt";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Status";
    readonly symbol: "snt";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Storj";
    readonly symbol: "storj";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Swarm City";
    readonly symbol: "swt";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "TrueUSD";
    readonly symbol: "tusd";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Wings";
    readonly symbol: "wings";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "0x";
    readonly symbol: "zrx";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Expanse";
    readonly symbol: "exp";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Viberate";
    readonly symbol: "vib";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Odyssey";
    readonly symbol: "ocn";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Polymath";
    readonly symbol: "poly";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Storm";
    readonly symbol: "storm";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Nano";
    readonly symbol: "nano";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "RaiBlocks";
    readonly symbol: "xrb";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Siacoin";
    readonly symbol: "sc";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "HyperSpace";
    readonly symbol: "xsc";
} | {
    readonly validate: (address: string, opts?: import("./types").OptsNetworkTypeOptional<"prod" | "testnet" | "stagenet" | "both"> | undefined) => boolean;
    readonly name: "loki";
    readonly symbol: "loki";
    readonly addressTypes: {
        readonly prod: readonly ["114", "115", "116"];
        readonly testnet: readonly [];
    };
    readonly iAddressTypes: {
        readonly prod: readonly ["115"];
        readonly testnet: readonly [];
    };
} | {
    readonly validate: (address: string, opts?: {
        networkType?: ("prod" | "testnet") | undefined;
    } | undefined) => boolean;
    readonly name: "LBRY Credits";
    readonly symbol: "lbc";
    readonly addressTypes: {
        readonly prod: readonly ["55"];
        readonly testnet: readonly [];
    };
} | {
    readonly validate: (address: string, opts?: import("./types").OptsNetworkTypeOptional<"prod" | "testnet"> | undefined) => boolean;
    readonly name: "Tron";
    readonly symbol: "trx";
    readonly addressTypes: {
        readonly prod: readonly [65];
        readonly testnet: readonly [160];
    };
} | {
    readonly validate: (_address: string) => boolean;
    readonly name: "Nem";
    readonly symbol: "xem";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Lisk";
    readonly symbol: "lsk";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Stellar";
    readonly symbol: "xlm";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "BTU Protocol";
    readonly symbol: "btu";
} | {
    readonly validate: (address: string, opts?: import("./types").OptsNetworkTypeOptional<"prod" | "testnet"> | undefined) => boolean;
    readonly name: "Crypto.com Coin";
    readonly symbol: "cro";
    readonly bech32Hrp: {
        readonly prod: readonly ["cro"];
        readonly testnet: readonly ["tcro"];
    };
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Multi-collateral DAI";
    readonly symbol: "dai";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Enjin Coin";
    readonly symbol: "enj";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "HedgeTrade";
    readonly symbol: "hedg";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Cred";
    readonly symbol: "lba";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Chainlink";
    readonly symbol: "link";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Loom Network";
    readonly symbol: "loom";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Maker";
    readonly symbol: "mkr";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Metal";
    readonly symbol: "mtl";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Ocean Protocol";
    readonly symbol: "ocean";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Quant";
    readonly symbol: "qnt";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Synthetix Network";
    readonly symbol: "snx";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "SOLVE";
    readonly symbol: "solve";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Spendcoin";
    readonly symbol: "spnd";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "TEMCO";
    readonly symbol: "temco";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "EOS";
    readonly symbol: "eos";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Tezos";
    readonly symbol: "xtz";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "VeChain";
    readonly symbol: "vet";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "StormX";
    readonly symbol: "stmx";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "AugurV2";
    readonly symbol: "repv2";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "FirmaChain";
    readonly symbol: "fct";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "BlockTrade";
    readonly symbol: "btt";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Quantum Resistant Ledger";
    readonly symbol: "qrl";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Serve";
    readonly symbol: "serv";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Tap";
    readonly symbol: "xtp";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Compound";
    readonly symbol: "comp";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Paxos";
    readonly symbol: "pax";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "USD Coin";
    readonly symbol: "usdc";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "CUSD";
    readonly symbol: "cusd";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Algorand";
    readonly symbol: "algo";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Polkadot";
    readonly symbol: "dot";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Uniswap Coin";
    readonly symbol: "uni";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Aave Coin";
    readonly symbol: "aave";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Matic";
    readonly symbol: "matic";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Decentraland";
    readonly symbol: "mana";
} | {
    readonly validate: (address: string) => boolean;
    readonly name: "Solana";
    readonly symbol: "sol";
    readonly maxLength: 44;
    readonly minLength: 43;
};
declare function getSupportedSymbols(): Array<string>;
export { currencies, getCurrency, getSupportedSymbols, };
export type { CurrencySymbolAnyRegister, CurrencyNameAnyRegister, ValidateOpts, };
