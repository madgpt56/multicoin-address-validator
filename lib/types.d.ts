export declare type OptsNetworkTypeOptional<T extends string = string> = {
    networkType?: T;
};
export declare type ExtractNetworkType<U> = U extends {
    addressTypes: unknown;
} ? keyof U['addressTypes'] : never;
