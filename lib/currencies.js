"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getSupportedSymbols = exports.getCurrency = exports.currencies = void 0;
const btc_1 = require("./currencies/btc");
const ltc_1 = require("./currencies/ltc");
const xrp_1 = require("./currencies/xrp");
const trx_1 = require("./currencies/trx");
const bsv_1 = require("./currencies/bsv");
const bch_1 = require("./currencies/bch");
const sol_1 = require("./currencies/sol");
const cro_1 = require("./currencies/cro");
const dot_1 = require("./currencies/dot");
const algo_1 = require("./currencies/algo");
const usdt_1 = require("./currencies/usdt");
const xtz_1 = require("./currencies/xtz");
const eos_1 = require("./currencies/eos");
const xlm_1 = require("./currencies/xlm");
const lsk_1 = require("./currencies/lsk");
const xem_1 = require("./currencies/xem");
const xsc_1 = require("./currencies/xsc");
const sc_1 = require("./currencies/sc");
const xrb_1 = require("./currencies/xrb");
const nano_1 = require("./currencies/nano");
const loki_1 = require("./currencies/loki");
const xmr_1 = require("./currencies/xmr");
const ada_1 = require("./currencies/ada");
const bkx_1 = require("./currencies/bkx");
const clo_1 = require("./currencies/clo");
const etc_1 = require("./currencies/etc");
const etz_1 = require("./currencies/etz");
const eth_1 = require("./currencies/eth");
const game_1 = require("./currencies/game");
const mana_1 = require("./currencies/mana");
const matic_1 = require("./currencies/matic");
const aave_1 = require("./currencies/aave");
const uni_1 = require("./currencies/uni");
const cusd_1 = require("./currencies/cusd");
const usdc_1 = require("./currencies/usdc");
const pax_1 = require("./currencies/pax");
const comp_1 = require("./currencies/comp");
const xtp_1 = require("./currencies/xtp");
const serv_1 = require("./currencies/serv");
const qrl_1 = require("./currencies/qrl");
const btt_1 = require("./currencies/btt");
const fct_1 = require("./currencies/fct");
const repv2_1 = require("./currencies/repv2");
const stmx_1 = require("./currencies/stmx");
const vet_1 = require("./currencies/vet");
const temco_1 = require("./currencies/temco");
const spnd_1 = require("./currencies/spnd");
const solve_1 = require("./currencies/solve");
const snx_1 = require("./currencies/snx");
const qnt_1 = require("./currencies/qnt");
const ocean_1 = require("./currencies/ocean");
const mtl_1 = require("./currencies/mtl");
const mkr_1 = require("./currencies/mkr");
const loom_1 = require("./currencies/loom");
const link_1 = require("./currencies/link");
const lba_1 = require("./currencies/lba");
const hedg_1 = require("./currencies/hedg");
const enj_1 = require("./currencies/enj");
const dai_1 = require("./currencies/dai");
const btu_1 = require("./currencies/btu");
const storm_1 = require("./currencies/storm");
const poly_1 = require("./currencies/poly");
const ocn_1 = require("./currencies/ocn");
const vib_1 = require("./currencies/vib");
const exp_1 = require("./currencies/exp");
const zrx_1 = require("./currencies/zrx");
const wings_1 = require("./currencies/wings");
const tusd_1 = require("./currencies/tusd");
const swt_1 = require("./currencies/swt");
const storj_1 = require("./currencies/storj");
const snt_1 = require("./currencies/snt");
const salt_1 = require("./currencies/salt");
const rlc_1 = require("./currencies/rlc");
const rep_1 = require("./currencies/rep");
const rcn_1 = require("./currencies/rcn");
const pay_1 = require("./currencies/pay");
const omg_1 = require("./currencies/omg");
const nmr_1 = require("./currencies/nmr");
const mln_1 = require("./currencies/mln");
const gup_1 = require("./currencies/gup");
const glm_1 = require("./currencies/glm");
const gnt_1 = require("./currencies/gnt");
const gno_1 = require("./currencies/gno");
const dnt_1 = require("./currencies/dnt");
const cvc_1 = require("./currencies/cvc");
const bnt_1 = require("./currencies/bnt");
const bat_1 = require("./currencies/bat");
const ant_1 = require("./currencies/ant");
const waves_1 = require("./currencies/waves");
const lbc_1 = require("./currencies/lbc");
const qtum_1 = require("./currencies/qtum");
const gas_1 = require("./currencies/gas");
const neo_1 = require("./currencies/neo");
const dash_1 = require("./currencies/dash");
const dgb_1 = require("./currencies/dgb");
const mona_1 = require("./currencies/mona");
const slr_1 = require("./currencies/slr");
const pivx_1 = require("./currencies/pivx");
const dcr_1 = require("./currencies/dcr");
const vot_1 = require("./currencies/vot");
const zen_1 = require("./currencies/zen");
const zcl_1 = require("./currencies/zcl");
const zec_1 = require("./currencies/zec");
const sng_1 = require("./currencies/sng");
const hush_1 = require("./currencies/hush");
const btcp_1 = require("./currencies/btcp");
const btcz_1 = require("./currencies/btcz");
const kmd_1 = require("./currencies/kmd");
const btg_1 = require("./currencies/btg");
const vtc_1 = require("./currencies/vtc");
const grlc_1 = require("./currencies/grlc");
const bio_1 = require("./currencies/bio");
const nmc_1 = require("./currencies/nmc");
const aur_1 = require("./currencies/aur");
const xpm_1 = require("./currencies/xpm");
const mec_1 = require("./currencies/mec");
const pts_1 = require("./currencies/pts");
const frc_1 = require("./currencies/frc");
const bvc_1 = require("./currencies/bvc");
const doge_1 = require("./currencies/doge");
const ppc_1 = require("./currencies/ppc");
const currencies = [
    Object.assign(Object.assign({}, btc_1.btcCurrency), { validate: btc_1.btcValidate }),
    Object.assign(Object.assign({}, bch_1.bchCurrency), { validate: bch_1.bchValidate }),
    Object.assign(Object.assign({}, bsv_1.bsvCurrency), { validate: bsv_1.bsvValidate }),
    Object.assign(Object.assign({}, ltc_1.ltcCurrency), { validate: ltc_1.ltcValidate }),
    Object.assign(Object.assign({}, ppc_1.ppcCurrency), { validate: ppc_1.ppcValidate }),
    Object.assign(Object.assign({}, doge_1.dogeCurrency), { validate: doge_1.dogeValidate }),
    Object.assign(Object.assign({}, bvc_1.bvcCurrency), { validate: bvc_1.bvcValidate }),
    Object.assign(Object.assign({}, frc_1.frcCurrency), { validate: frc_1.frcValidate }),
    Object.assign(Object.assign({}, pts_1.ptsCurrency), { validate: pts_1.ptsValidate }),
    Object.assign(Object.assign({}, mec_1.mecCurrency), { validate: mec_1.mecValidate }),
    Object.assign(Object.assign({}, xpm_1.xpmCurrency), { validate: xpm_1.xpmValidate }),
    Object.assign(Object.assign({}, aur_1.aurCurrency), { validate: aur_1.aurValidate }),
    Object.assign(Object.assign({}, nmc_1.nmcCurrency), { validate: nmc_1.nmcValidate }),
    Object.assign(Object.assign({}, bio_1.bioCurrency), { validate: bio_1.bioValidate }),
    Object.assign(Object.assign({}, grlc_1.grlcCurrency), { validate: grlc_1.grlcValidate }),
    Object.assign(Object.assign({}, vtc_1.vtcCurrency), { validate: vtc_1.vtcValidate }),
    Object.assign(Object.assign({}, btg_1.btgCurrency), { validate: btg_1.btgValidate }),
    Object.assign(Object.assign({}, kmd_1.kmdCurrency), { validate: kmd_1.kmdValidate }),
    Object.assign(Object.assign({}, btcz_1.btczCurrency), { validate: btcz_1.btczValidate }),
    Object.assign(Object.assign({}, btcp_1.btcpCurrency), { validate: btcp_1.btcpValidate }),
    Object.assign(Object.assign({}, hush_1.hushCurrency), { validate: hush_1.hushValidate }),
    Object.assign(Object.assign({}, sng_1.sngCurrency), { validate: sng_1.sngValidate }),
    Object.assign(Object.assign({}, zec_1.zecCurrency), { validate: zec_1.zecValidate }),
    Object.assign(Object.assign({}, zcl_1.zclCurrency), { validate: zcl_1.zclValidate }),
    Object.assign(Object.assign({}, zen_1.zenCurrency), { validate: zen_1.zenValidate }),
    Object.assign(Object.assign({}, vot_1.votCurrency), { validate: vot_1.votValidate }),
    Object.assign(Object.assign({}, dcr_1.dcrCurrency), { validate: dcr_1.dcrValidate }),
    Object.assign(Object.assign({}, game_1.gameCurrency), { validate: game_1.gameValidate }),
    Object.assign(Object.assign({}, pivx_1.pivxCurrency), { validate: pivx_1.pivxValidate }),
    Object.assign(Object.assign({}, slr_1.slrCurrency), { validate: slr_1.slrValidate }),
    Object.assign(Object.assign({}, mona_1.monaCurrency), { validate: mona_1.monaValidate }),
    Object.assign(Object.assign({}, dgb_1.dgbCurrency), { validate: dgb_1.dgbValidate }),
    Object.assign(Object.assign({}, usdt_1.usdtCurrency), { validate: usdt_1.usdtValidate }),
    Object.assign(Object.assign({}, xrp_1.xrpCurrency), { validate: xrp_1.xrpValidate }),
    Object.assign(Object.assign({}, dash_1.dashCurrency), { validate: dash_1.dashValidate }),
    Object.assign(Object.assign({}, neo_1.neoCurrency), { validate: neo_1.neoValidate }),
    Object.assign(Object.assign({}, gas_1.gasCurrency), { validate: gas_1.gasValidate }),
    Object.assign(Object.assign({}, qtum_1.qtumCurrency), { validate: qtum_1.qtumValidate }),
    Object.assign(Object.assign({}, waves_1.wavesCurrency), { validate: waves_1.wavesValidate }),
    Object.assign(Object.assign({}, eth_1.ethCurrency), { validate: eth_1.ethValidate }),
    Object.assign(Object.assign({}, etz_1.etzCurrency), { validate: etz_1.etzValidate }),
    Object.assign(Object.assign({}, etc_1.etcCurrency), { validate: etc_1.etcValidate }),
    Object.assign(Object.assign({}, clo_1.cloCurrency), { validate: clo_1.cloValidate }),
    Object.assign(Object.assign({}, bkx_1.bkxCurrency), { validate: bkx_1.bkxValidate }),
    Object.assign(Object.assign({}, ada_1.adaCurrency), { validate: ada_1.adaValidate }),
    Object.assign(Object.assign({}, xmr_1.xmrCurrency), { validate: xmr_1.xmrValidate }),
    Object.assign(Object.assign({}, ant_1.antCurrency), { validate: ant_1.antValidate }),
    Object.assign(Object.assign({}, bat_1.batCurrency), { validate: bat_1.batValidate }),
    Object.assign(Object.assign({}, bnt_1.bntCurrency), { validate: bnt_1.bntValidate }),
    Object.assign(Object.assign({}, cvc_1.cvcCurrency), { validate: cvc_1.cvcValidate }),
    Object.assign(Object.assign({}, dnt_1.dntCurrency), { validate: dnt_1.dntValidate }),
    Object.assign(Object.assign({}, gno_1.gnoCurrency), { validate: gno_1.gnoValidate }),
    Object.assign(Object.assign({}, gnt_1.gntCurrency), { validate: gnt_1.gntValidate }),
    Object.assign(Object.assign({}, glm_1.glmCurrency), { validate: glm_1.glmValidate }),
    Object.assign(Object.assign({}, gup_1.gupCurrency), { validate: gup_1.gupValidate }),
    Object.assign(Object.assign({}, mln_1.mlnCurrency), { validate: mln_1.mlnValidate }),
    Object.assign(Object.assign({}, nmr_1.nmrCurrency), { validate: nmr_1.nmrValidate }),
    Object.assign(Object.assign({}, omg_1.omgCurrency), { validate: omg_1.omgValidate }),
    Object.assign(Object.assign({}, pay_1.payCurrency), { validate: pay_1.payValidate }),
    Object.assign(Object.assign({}, rcn_1.rcnCurrency), { validate: rcn_1.rcnValidate }),
    Object.assign(Object.assign({}, rep_1.repCurrency), { validate: rep_1.repValidate }),
    Object.assign(Object.assign({}, rlc_1.rlcCurrency), { validate: rlc_1.rlcValidate }),
    Object.assign(Object.assign({}, salt_1.saltCurrency), { validate: salt_1.saltValidate }),
    Object.assign(Object.assign({}, snt_1.sntCurrency), { validate: snt_1.sntValidate }),
    Object.assign(Object.assign({}, storj_1.storjCurrency), { validate: storj_1.storjValidate }),
    Object.assign(Object.assign({}, swt_1.swtCurrency), { validate: swt_1.swtValidate }),
    Object.assign(Object.assign({}, tusd_1.tusdCurrency), { validate: tusd_1.tusdValidate }),
    Object.assign(Object.assign({}, wings_1.wingsCurrency), { validate: wings_1.wingsValidate }),
    Object.assign(Object.assign({}, zrx_1.zrxCurrency), { validate: zrx_1.zrxValidate }),
    Object.assign(Object.assign({}, exp_1.expCurrency), { validate: exp_1.expValidate }),
    Object.assign(Object.assign({}, vib_1.vibCurrency), { validate: vib_1.vibValidate }),
    Object.assign(Object.assign({}, ocn_1.ocnCurrency), { validate: ocn_1.ocnValidate }),
    Object.assign(Object.assign({}, poly_1.polyCurrency), { validate: poly_1.polyValidate }),
    Object.assign(Object.assign({}, storm_1.stormCurrency), { validate: storm_1.stormValidate }),
    Object.assign(Object.assign({}, nano_1.nanoCurrency), { validate: nano_1.nanoValidate }),
    Object.assign(Object.assign({}, xrb_1.xrbCurrency), { validate: xrb_1.xrbValidate }),
    Object.assign(Object.assign({}, sc_1.scCurrency), { validate: sc_1.scValidate }),
    Object.assign(Object.assign({}, xsc_1.xscCurrency), { validate: xsc_1.xscValidate }),
    Object.assign(Object.assign({}, loki_1.lokiCurrency), { validate: loki_1.lokiValidate }),
    Object.assign(Object.assign({}, lbc_1.lbcCurrency), { validate: lbc_1.lbcValidate }),
    Object.assign(Object.assign({}, trx_1.trxCurrency), { validate: trx_1.trxValidate }),
    Object.assign(Object.assign({}, xem_1.xemCurrency), { validate: xem_1.xemValidate }),
    Object.assign(Object.assign({}, lsk_1.lskCurrency), { validate: lsk_1.lskValidate }),
    Object.assign(Object.assign({}, xlm_1.xlmCurrency), { validate: xlm_1.xlmValidate }),
    Object.assign(Object.assign({}, btu_1.btuCurrency), { validate: btu_1.btuValidate }),
    Object.assign(Object.assign({}, cro_1.croCurrency), { validate: cro_1.croValidate }),
    Object.assign(Object.assign({}, dai_1.daiCurrency), { validate: dai_1.daiValidate }),
    Object.assign(Object.assign({}, enj_1.enjCurrency), { validate: enj_1.enjValidate }),
    Object.assign(Object.assign({}, hedg_1.hedgCurrency), { validate: hedg_1.hedgValidate }),
    Object.assign(Object.assign({}, lba_1.lbaCurrency), { validate: lba_1.lbaValidate }),
    Object.assign(Object.assign({}, link_1.linkCurrency), { validate: link_1.linkValidate }),
    Object.assign(Object.assign({}, loom_1.loomCurrency), { validate: loom_1.loomValidate }),
    Object.assign(Object.assign({}, mkr_1.mkrCurrency), { validate: mkr_1.mkrValidate }),
    Object.assign(Object.assign({}, mtl_1.mtlCurrency), { validate: mtl_1.mtlValidate }),
    Object.assign(Object.assign({}, ocean_1.oceanCurrency), { validate: ocean_1.oceanValidate }),
    Object.assign(Object.assign({}, qnt_1.qntCurrency), { validate: qnt_1.qntValidate }),
    Object.assign(Object.assign({}, snx_1.snxCurrency), { validate: snx_1.snxValidate }),
    Object.assign(Object.assign({}, solve_1.solveCurrency), { validate: solve_1.solveValidate }),
    Object.assign(Object.assign({}, spnd_1.spndCurrency), { validate: spnd_1.spndValidate }),
    Object.assign(Object.assign({}, temco_1.temcoCurrency), { validate: temco_1.temcoValidate }),
    Object.assign(Object.assign({}, eos_1.eosCurrency), { validate: eos_1.eosValidate }),
    Object.assign(Object.assign({}, xtz_1.xtzCurrency), { validate: xtz_1.xtzValidate }),
    Object.assign(Object.assign({}, vet_1.vetCurrency), { validate: vet_1.vetValidate }),
    Object.assign(Object.assign({}, stmx_1.stmxCurrency), { validate: stmx_1.stmxValidate }),
    Object.assign(Object.assign({}, repv2_1.repv2Currency), { validate: repv2_1.repv2Validate }),
    Object.assign(Object.assign({}, fct_1.fctCurrency), { validate: fct_1.fctValidate }),
    Object.assign(Object.assign({}, btt_1.bttCurrency), { validate: btt_1.bttValidate }),
    Object.assign(Object.assign({}, qrl_1.qrlCurrency), { validate: qrl_1.qrlValidate }),
    Object.assign(Object.assign({}, serv_1.servCurrency), { validate: serv_1.servValidate }),
    Object.assign(Object.assign({}, xtp_1.xtpCurrency), { validate: xtp_1.xtpValidate }),
    Object.assign(Object.assign({}, comp_1.compCurrency), { validate: comp_1.compValidate }),
    Object.assign(Object.assign({}, pax_1.paxCurrency), { validate: pax_1.paxValidate }),
    Object.assign(Object.assign({}, usdc_1.usdcCurrency), { validate: usdc_1.usdcValidate }),
    Object.assign(Object.assign({}, cusd_1.cusdCurrency), { validate: cusd_1.cusdValidate }),
    Object.assign(Object.assign({}, algo_1.algoCurrency), { validate: algo_1.algoValidate }),
    Object.assign(Object.assign({}, dot_1.dotCurrency), { validate: dot_1.dotValidate }),
    Object.assign(Object.assign({}, uni_1.uniCurrency), { validate: uni_1.uniValidate }),
    Object.assign(Object.assign({}, aave_1.aaveCurrency), { validate: aave_1.aaveValidate }),
    Object.assign(Object.assign({}, matic_1.maticCurrency), { validate: matic_1.maticValidate }),
    Object.assign(Object.assign({}, mana_1.manaCurrency), { validate: mana_1.manaValidate }),
    Object.assign(Object.assign({}, sol_1.solCurrency), { validate: sol_1.solValidate }),
];
exports.currencies = currencies;
const currenciesBySymbol = Object.fromEntries(currencies.map((currency) => [currency.symbol, currency]));
const currenciesByLowercaseName = Object.fromEntries(currencies.map((currency) => [currency.name.toLowerCase(), currency]));
function getCurrency(symbolOrName) {
    var _a;
    const lowerCased = symbolOrName === null || symbolOrName === void 0 ? void 0 : symbolOrName.toLowerCase();
    return (_a = currenciesBySymbol[lowerCased]) !== null && _a !== void 0 ? _a : currenciesByLowercaseName[lowerCased];
}
exports.getCurrency = getCurrency;
function getSupportedSymbols() {
    return currencies.map((currency) => currency.symbol);
}
exports.getSupportedSymbols = getSupportedSymbols;
/// /spit out details for readme.md
// CURRENCIES
//     .sort((a, b) => a.name.toUpperCase() > b.name.toUpperCase() ? 1 : -1)
//     .forEach(c => console.log(`* ${c.name}/${c.symbol} \`'${c.name}'\` or \`'${c.symbol}'\` `));
/// /spit out keywords for package.json
// CURRENCIES
//     .sort((a, b) => a.name.toUpperCase() > b.name.toUpperCase() ? 1 : -1)
//     .forEach(c => console.log(`"${c.name}","${c.symbol}",`));
//
//# sourceMappingURL=currencies.js.map