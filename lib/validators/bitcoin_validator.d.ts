import { ExtractNetworkType, OptsNetworkTypeOptional } from '../types';
declare type BtcCurrency = typeof import('../currencies/btc').btcCurrency;
declare type LtcCurrency = typeof import('../currencies/ltc').ltcCurrency;
declare type PpcCurrency = typeof import('../currencies/ppc').ppcCurrency;
declare type DogeCurrency = typeof import('../currencies/doge').dogeCurrency;
declare type BvcCurrency = typeof import('../currencies/bvc').bvcCurrency;
declare type FrcCurrency = typeof import('../currencies/frc').frcCurrency;
declare type PtsCurrency = typeof import('../currencies/pts').ptsCurrency;
declare type MecCurrency = typeof import('../currencies/mec').mecCurrency;
declare type XpmCurrency = typeof import('../currencies/xpm').xpmCurrency;
declare type AurCurrency = typeof import('../currencies/aur').aurCurrency;
declare type NmcCurrency = typeof import('../currencies/nmc').nmcCurrency;
declare type BioCurrency = typeof import('../currencies/bio').bioCurrency;
declare type GrlcCurrency = typeof import('../currencies/grlc').grlcCurrency;
declare type VtcCurrency = typeof import('../currencies/vtc').vtcCurrency;
declare type BtgCurrency = typeof import('../currencies/btg').btgCurrency;
declare type KmdCurrency = typeof import('../currencies/kmd').kmdCurrency;
declare type BtczCurrency = typeof import('../currencies/btcz').btczCurrency;
declare type BtcpCurrency = typeof import('../currencies/btcp').btcpCurrency;
declare type HushCurrency = typeof import('../currencies/hush').hushCurrency;
declare type SngCurrency = typeof import('../currencies/sng').sngCurrency;
declare type ZecCurrency = typeof import('../currencies/zec').zecCurrency;
declare type ZclCurrency = typeof import('../currencies/zcl').zclCurrency;
declare type ZenCurrency = typeof import('../currencies/zen').zenCurrency;
declare type VotCurrency = typeof import('../currencies/vot').votCurrency;
declare type DcrCurrency = typeof import('../currencies/dcr').dcrCurrency;
declare type PivxCurrency = typeof import('../currencies/pivx').pivxCurrency;
declare type SlrCurrency = typeof import('../currencies/slr').slrCurrency;
declare type MonaCurrency = typeof import('../currencies/mona').monaCurrency;
declare type DgbCurrency = typeof import('../currencies/dgb').dgbCurrency;
declare type DashCurrency = typeof import('../currencies/dash').dashCurrency;
declare type NeoCurrency = typeof import('../currencies/neo').neoCurrency;
declare type GasCurrency = typeof import('../currencies/gas').gasCurrency;
declare type QtumCurrency = typeof import('../currencies/qtum').qtumCurrency;
declare type LbcCurrency = typeof import('../currencies/lbc').lbcCurrency;
declare type WavesCurrency = typeof import('../currencies/waves').wavesCurrency;
declare type BchCurrency = typeof import('../currencies/bch').bchCurrency;
declare type BscCurrency = typeof import('../currencies/bsv').bsvCurrency;
declare type UsdtCurrency = typeof import('../currencies/usdt').usdtCurrency;
declare type Currency = BtcCurrency | LtcCurrency | PpcCurrency | DogeCurrency | BvcCurrency | FrcCurrency | PtsCurrency | MecCurrency | XpmCurrency | AurCurrency | NmcCurrency | BioCurrency | GrlcCurrency | VtcCurrency | BtgCurrency | KmdCurrency | BtczCurrency | BtcpCurrency | HushCurrency | SngCurrency | ZecCurrency | ZclCurrency | ZenCurrency | VotCurrency | DcrCurrency | PivxCurrency | SlrCurrency | MonaCurrency | DgbCurrency | DashCurrency | NeoCurrency | GasCurrency | QtumCurrency | LbcCurrency | WavesCurrency | BchCurrency | BscCurrency | UsdtCurrency;
declare type CurrencyNetworkType = ExtractNetworkType<Currency>;
declare type Opts = OptsNetworkTypeOptional<CurrencyNetworkType>;
declare const BTCValidator: {
    isValidAddress(address: string, currency: Currency, opts?: Opts): boolean;
};
declare type ValidatorParams = Parameters<typeof BTCValidator.isValidAddress>;
export { BTCValidator };
export type { ValidatorParams };
