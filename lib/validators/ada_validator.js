"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ADAValidator = void 0;
// @ts-ignore
const cbor_js_1 = __importDefault(require("cbor-js"));
const crc_1 = require("crc");
const bip173_validator_1 = require("./bip173_validator");
const base58Decode_1 = require("../utils/base58Decode");
const cborDecode = cbor_js_1.default.decode;
function getDecoded(address) {
    try {
        const decoded = (0, base58Decode_1.base58Decode)(address);
        return cborDecode(new Uint8Array(decoded).buffer);
    }
    catch (e) {
        // if decoding fails, assume invalid address
        return null;
    }
}
function isValidAddressV1(address) {
    const decoded = getDecoded(address);
    if (!decoded || (!Array.isArray(decoded) || decoded.length !== 2)) {
        return false;
    }
    const [tagged, validCrc] = decoded;
    if (typeof (validCrc) !== 'number') {
        return false;
    }
    // get crc of the payload
    const crc = (0, crc_1.crc32)(tagged); // todo check CRC types
    return crc === validCrc;
}
function isValidAddressShelley(address, currency, opts) {
    // shelley address are just bip 173 - bech32 addresses (https://cips.cardano.org/cips/cip4/)
    return bip173_validator_1.BIP173Validator.isValidAddress(address, currency, opts);
}
const ADAValidator = {
    isValidAddress(address, currency, opts = {}) {
        return isValidAddressV1(address) || isValidAddressShelley(address, currency, opts);
    },
};
exports.ADAValidator = ADAValidator;
//# sourceMappingURL=ada_validator.js.map