"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.NANOValidator = void 0;
const base_x_1 = __importDefault(require("base-x"));
const toHex_1 = require("../utils/toHex");
const blake2b_1 = require("../utils/blake2b");
const ALLOWED_CHARS = '13456789abcdefghijkmnopqrstuwxyz';
const codec = (0, base_x_1.default)(ALLOWED_CHARS);
// https://github.com/nanocurrency/raiblocks/wiki/Accounts,-Keys,-Seeds,-and-Wallet-Identifiers
const regexp = new RegExp(`^(xrb|nano)_([${ALLOWED_CHARS}]{60})$`);
function verifyChecksum(address) {
    const bytes = codec.decode(regexp.exec(address)[2]).slice(-37);
    // https://github.com/nanocurrency/raiblocks/blob/master/rai/lib/numbers.cpp#L73
    const computedChecksum = (0, blake2b_1.blake2b)((0, toHex_1.toHex)(bytes.slice(0, -5)), 5);
    const checksum = (0, toHex_1.toHex)(bytes.slice(-5).reverse());
    return computedChecksum === checksum;
}
const NANOValidator = {
    isValidAddress(address) {
        if (regexp.test(address)) {
            return verifyChecksum(address);
        }
        return false;
    },
};
exports.NANOValidator = NANOValidator;
//# sourceMappingURL=nano_validator.js.map