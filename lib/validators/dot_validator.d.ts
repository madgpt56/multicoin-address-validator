declare const DotValidator: {
    isValidAddress(address: string): boolean;
};
declare type ValidatorParams = Parameters<typeof DotValidator.isValidAddress>;
export { DotValidator };
export type { ValidatorParams };
