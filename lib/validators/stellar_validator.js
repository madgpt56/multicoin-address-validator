"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.XLMValidator = void 0;
const base_x_1 = __importDefault(require("base-x"));
const crc_1 = require("crc");
const numberToHex_1 = require("../utils/numberToHex");
const toHex_1 = require("../utils/toHex");
const ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567';
const base32 = (0, base_x_1.default)(ALPHABET);
const regexp = new RegExp(`^[${ALPHABET}]{56}$`);
const ed25519PublicKeyVersionByte = (6 << 3);
function swap16(number) {
    const lower = number & 0xFF;
    const upper = (number >> 8) & 0xFF;
    return (lower << 8) | upper;
}
function verifyChecksum(address) {
    // based on https://github.com/stellar/js-stellar-base/blob/master/src/strkey.js#L126
    const bytes = base32.decode(address);
    if (bytes[0] !== ed25519PublicKeyVersionByte) {
        return false;
    }
    const computedChecksum = (0, numberToHex_1.numberToHex)(swap16((0, crc_1.crc16xmodem)(bytes.slice(0, -2))), 4);
    const checksum = (0, toHex_1.toHex)(bytes.slice(-2));
    return computedChecksum === checksum;
}
const XLMValidator = {
    isValidAddress(address) {
        if (regexp.test(address)) {
            return verifyChecksum(address);
        }
        return false;
    },
};
exports.XLMValidator = XLMValidator;
//# sourceMappingURL=stellar_validator.js.map