declare const NANOValidator: {
    isValidAddress(address: string): boolean;
};
declare type ValidatorParams = Parameters<typeof NANOValidator.isValidAddress>;
export { NANOValidator };
export type { ValidatorParams };
