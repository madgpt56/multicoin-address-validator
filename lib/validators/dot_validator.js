"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DotValidator = void 0;
const byteArray2hexStr_1 = require("../utils/byteArray2hexStr");
const blake2b_1 = require("../utils/blake2b");
const base58Decode_1 = require("../utils/base58Decode");
// from https://github.com/paritytech/substrate/wiki/External-Address-Format-(SS58)
const addressFormats = [
    { addressLength: 3, accountIndexLength: 1, checkSumLength: 1 },
    { addressLength: 4, accountIndexLength: 2, checkSumLength: 1 },
    { addressLength: 5, accountIndexLength: 2, checkSumLength: 2 },
    { addressLength: 6, accountIndexLength: 4, checkSumLength: 1 },
    { addressLength: 7, accountIndexLength: 4, checkSumLength: 2 },
    { addressLength: 8, accountIndexLength: 4, checkSumLength: 3 },
    { addressLength: 9, accountIndexLength: 4, checkSumLength: 4 },
    { addressLength: 10, accountIndexLength: 8, checkSumLength: 1 },
    { addressLength: 11, accountIndexLength: 8, checkSumLength: 2 },
    { addressLength: 12, accountIndexLength: 8, checkSumLength: 3 },
    { addressLength: 13, accountIndexLength: 8, checkSumLength: 4 },
    { addressLength: 14, accountIndexLength: 8, checkSumLength: 5 },
    { addressLength: 15, accountIndexLength: 8, checkSumLength: 6 },
    { addressLength: 16, accountIndexLength: 8, checkSumLength: 7 },
    { addressLength: 17, accountIndexLength: 8, checkSumLength: 8 },
    { addressLength: 34, accountIndexLength: 32, checkSumLength: 2 },
];
function verifyChecksum(address) {
    try {
        const preImage = '53533538505245';
        const decoded = (0, base58Decode_1.base58Decode)(address);
        const addressType = (0, byteArray2hexStr_1.byteArray2hexStr)(decoded.slice(0, 1));
        const addressAndChecksum = decoded.slice(1);
        // get the address format
        const addressFormat = addressFormats.find((af) => af.addressLength === addressAndChecksum.length);
        if (!addressFormat) {
            return false;
        }
        const decodedAddress = (0, byteArray2hexStr_1.byteArray2hexStr)(addressAndChecksum.slice(0, addressFormat.accountIndexLength));
        const checksum = (0, byteArray2hexStr_1.byteArray2hexStr)(addressAndChecksum.slice(-addressFormat.checkSumLength));
        const calculatedHash = (0, blake2b_1.blake2b)(preImage + addressType + decodedAddress, 64)
            .slice(0, addressFormat.checkSumLength * 2)
            .toUpperCase();
        return calculatedHash === checksum;
    }
    catch (err) {
        return false;
    }
}
const DotValidator = {
    isValidAddress(address) {
        return verifyChecksum(address);
    },
};
exports.DotValidator = DotValidator;
//# sourceMappingURL=dot_validator.js.map