declare type SolCurrency = typeof import('../currencies/sol').solCurrency;
declare type Currency = SolCurrency;
declare const Base58Validator: {
    isValidAddress(address: string, currency: Currency): boolean;
};
declare type ValidatorParams = Parameters<typeof Base58Validator.isValidAddress>;
export { Base58Validator };
export type { ValidatorParams };
