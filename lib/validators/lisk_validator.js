"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LSKValidator = void 0;
const buffer_1 = require("buffer");
const bigNumberToBuffer_1 = require("../utils/bigNumberToBuffer");
const regexp = /^[0-9]{1,20}L$/;
function verifyAddress(address) {
    const BUFFER_SIZE = 8;
    const bigNumber = address.substring(0, address.length - 1);
    const addressBuffer = (0, bigNumberToBuffer_1.bigNumberToBuffer)(bigNumber);
    return buffer_1.Buffer.from(addressBuffer).slice(0, BUFFER_SIZE).equals(addressBuffer);
}
const LSKValidator = {
    isValidAddress(address) {
        if (!regexp.test(address)) {
            return false;
        }
        return verifyAddress(address);
    },
};
exports.LSKValidator = LSKValidator;
//# sourceMappingURL=lisk_validator.js.map