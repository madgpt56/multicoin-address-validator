"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.XTZValidator = void 0;
const hexStr2byteArray_1 = require("../utils/hexStr2byteArray");
const sha256x2_1 = require("../utils/sha256x2");
const base58Decode_1 = require("../utils/base58Decode");
const byteArray2hexStr_1 = require("../utils/byteArray2hexStr");
const prefix = new Uint8Array([6, 161, 159]);
function decodeRaw(buffer) {
    const payload = buffer.slice(0, -4);
    const checksum = buffer.slice(-4);
    const newChecksum = (0, hexStr2byteArray_1.hexStr2byteArray)((0, sha256x2_1.sha256x2)((0, byteArray2hexStr_1.byteArray2hexStr)(payload)));
    if (checksum[0] ^ newChecksum[0]
        | checksum[1] ^ newChecksum[1]
        | checksum[2] ^ newChecksum[2]
        | checksum[3] ^ newChecksum[3])
        return false;
    return payload;
}
const XTZValidator = {
    isValidAddress(address) {
        try {
            const buffer = (0, base58Decode_1.base58Decode)(address);
            const payload = decodeRaw(buffer);
            if (!payload)
                return false;
            payload.slice(prefix.length);
            return true;
        }
        catch (e) {
            return false;
        }
    },
};
exports.XTZValidator = XTZValidator;
//# sourceMappingURL=tezos_validator.js.map