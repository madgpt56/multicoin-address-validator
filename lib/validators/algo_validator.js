"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AlgoValidator = void 0;
const byteArray2hexStr_1 = require("../utils/byteArray2hexStr");
const sha512_256_1 = require("../utils/sha512_256");
const base32_1 = require("../utils/base32");
const ALGORAND_CHECKSUM_BYTE_LENGTH = 4;
const ALGORAND_ADDRESS_LENGTH = 58;
function verifyChecksum(address) {
    if (address.length !== ALGORAND_ADDRESS_LENGTH) {
        return false;
    }
    // Decode base32 Address
    const decoded = (0, base32_1.base32Decode)(address);
    const addr = decoded.slice(0, decoded.length - ALGORAND_CHECKSUM_BYTE_LENGTH);
    const checksum = (0, byteArray2hexStr_1.byteArray2hexStr)(decoded.slice(-4));
    // Hash Address - Checksum
    const code = (0, sha512_256_1.sha512_256)((0, byteArray2hexStr_1.byteArray2hexStr)(addr)).slice(-ALGORAND_CHECKSUM_BYTE_LENGTH * 2);
    return code === checksum;
}
const AlgoValidator = {
    isValidAddress(address) {
        return verifyChecksum(address);
    },
};
exports.AlgoValidator = AlgoValidator;
//# sourceMappingURL=algo_validator.js.map