"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Base58Validator = void 0;
const base58Decode_1 = require("../utils/base58Decode");
// simple base58 validator.  Just checks if it can be decoded.
const Base58Validator = {
    isValidAddress(address, currency) {
        if (!address) {
            return false;
        }
        if (currency.minLength && (address.length < currency.minLength)) {
            return false;
        }
        if (currency.maxLength && (address.length > currency.maxLength)) {
            return false;
        }
        try {
            const decoded = (0, base58Decode_1.base58Decode)(address);
            return decoded.length > 0;
        }
        catch (e) {
            // if decoding fails, assume invalid address
            return false;
        }
    },
};
exports.Base58Validator = Base58Validator;
//# sourceMappingURL=base58_validator.js.map