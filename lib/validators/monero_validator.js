"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.XMRValidator = void 0;
const cnBase58_1 = require("../crypto/cnBase58");
const keccak256Checksum_1 = require("../utils/keccak256Checksum");
const DEFAULT_NETWORK_TYPE = 'prod';
const addressRegTest = /^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{95}$/;
const integratedAddressRegTest = /^[123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz]{106}$/;
function validateNetwork(decoded, currency, networkType, addressType) {
    let network = currency.addressTypes;
    if (addressType === 'integrated') {
        network = currency.iAddressTypes;
    }
    const at = parseInt(decoded.slice(0, 2), 16).toString();
    switch (networkType) {
        case 'prod':
            return network.prod.indexOf(at) >= 0;
        case 'testnet':
            return network.testnet.indexOf(at) >= 0;
        case 'stagenet':
            return network.stagenet.indexOf(at) >= 0;
        case 'both':
            return network.prod.indexOf(at) >= 0
                || network.testnet.indexOf(at) >= 0
                || network.stagenet.indexOf(at) >= 0;
        default:
            return false;
    }
}
function hextobin(hex) {
    const res = new Uint8Array(hex.length / 2);
    for (let i = 0; i < hex.length / 2; ++i) {
        res[i] = parseInt(hex.slice(i * 2, i * 2 + 2), 16);
    }
    return res;
}
const XMRValidator = {
    isValidAddress(address, currency, opts = {}) {
        const { networkType = DEFAULT_NETWORK_TYPE } = opts;
        let addressType = 'standard';
        if (!addressRegTest.test(address)) {
            if (integratedAddressRegTest.test(address)) {
                addressType = 'integrated';
            }
            else {
                return false;
            }
        }
        const decodedAddrStr = (0, cnBase58_1.cnBase58Decode)(address);
        if (!decodedAddrStr)
            return false;
        if (!validateNetwork(decodedAddrStr, currency, networkType, addressType))
            return false;
        const addrChecksum = decodedAddrStr.slice(-8);
        const hashChecksum = (0, keccak256Checksum_1.keccak256Checksum)(hextobin(decodedAddrStr.slice(0, -8)));
        return addrChecksum === hashChecksum;
    },
};
exports.XMRValidator = XMRValidator;
//# sourceMappingURL=monero_validator.js.map