import { OptsNetworkTypeOptional } from '../types';
declare type CroCurrency = typeof import('../currencies/cro').croCurrency;
declare type AdaCurrency = typeof import('../currencies/ada').adaCurrency;
declare type Currency = CroCurrency | AdaCurrency;
declare type CurrencyNetworkType = keyof CroCurrency['bech32Hrp'] | keyof AdaCurrency['bech32Hrp'];
declare const BIP173Validator: {
    isValidAddress(address: string, currency: Currency, opts?: OptsNetworkTypeOptional<CurrencyNetworkType>): boolean;
};
declare type ValidatorParams = Parameters<typeof BIP173Validator.isValidAddress>;
export { BIP173Validator };
export type { ValidatorParams };
