import { ExtractNetworkType, OptsNetworkTypeOptional } from '../types';
declare type BchCurrency = typeof import('../currencies/bch').bchCurrency;
declare type BscCurrency = typeof import('../currencies/bsv').bsvCurrency;
declare type Currency = BchCurrency | BscCurrency;
declare type CurrencyNetworkType = ExtractNetworkType<Currency>;
declare const BCHValidator: {
    isValidAddress(address: string, currency: Currency, opts?: OptsNetworkTypeOptional<CurrencyNetworkType> | undefined): boolean;
};
declare type ValidatorParams = Parameters<typeof BCHValidator.isValidAddress>;
export { BCHValidator };
export type { ValidatorParams };
