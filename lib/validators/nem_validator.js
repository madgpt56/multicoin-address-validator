"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NEMValidator = void 0;
const buffer_1 = require("buffer");
const base32_1 = require("../utils/base32");
const toHex_1 = require("../utils/toHex");
const keccak256Checksum_1 = require("../utils/keccak256Checksum");
const NEMValidator = {
    /**
     * Check if an address is valid
     *
     * @param {string} _address - An address
     *
     * @return {boolean} - True if address is valid, false otherwise
     */
    isValidAddress(_address) {
        const address = _address.toUpperCase().replace(/-/g, '');
        if (!address || address.length !== 40) {
            return false;
        }
        const decoded = (0, toHex_1.toHex)((0, base32_1.base32Decode)(address));
        const stepThreeChecksum = (0, keccak256Checksum_1.keccak256Checksum)(buffer_1.Buffer.from(decoded.slice(0, 42), 'hex'));
        return stepThreeChecksum === decoded.slice(42);
    },
};
exports.NEMValidator = NEMValidator;
//# sourceMappingURL=nem_validator.js.map