import { OptsNetworkTypeOptional } from '../types';
declare type TrxCurrency = typeof import('../currencies/trx').trxCurrency;
declare type Currency = TrxCurrency;
declare const TRXValidator: {
    /**
     * tron address validation
     */
    isValidAddress(mainAddress: string, currency: Currency, opts?: OptsNetworkTypeOptional<"prod" | "testnet"> | undefined): boolean;
};
declare type ValidatorParams = Parameters<typeof TRXValidator.isValidAddress>;
export { TRXValidator };
export type { ValidatorParams };
