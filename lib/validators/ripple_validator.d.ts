declare const XRPValidator: {
    /**
     * ripple address validation
     */
    isValidAddress(address: string): boolean;
};
declare type ValidatorParams = Parameters<typeof XRPValidator.isValidAddress>;
export { XRPValidator };
export type { ValidatorParams };
