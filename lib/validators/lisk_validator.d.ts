declare const LSKValidator: {
    isValidAddress(address: string): boolean;
};
declare type ValidatorParams = Parameters<typeof LSKValidator.isValidAddress>;
export { LSKValidator };
export type { ValidatorParams };
