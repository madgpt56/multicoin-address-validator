import { ExtractNetworkType, OptsNetworkTypeOptional } from '../types';
declare type UsdtCurrency = typeof import('../currencies/usdt').usdtCurrency;
declare type Currency = UsdtCurrency;
declare type CurrencyNetworkType = ExtractNetworkType<Currency>;
declare type Opts = OptsNetworkTypeOptional<CurrencyNetworkType> & {
    chainType?: 'erc20' | 'omni';
};
declare const USDTValidator: {
    isValidAddress(address: string, currency: Currency, opts?: Opts | undefined): boolean;
};
declare type ValidatorParams = Parameters<typeof USDTValidator.isValidAddress>;
export { USDTValidator };
export type { ValidatorParams };
