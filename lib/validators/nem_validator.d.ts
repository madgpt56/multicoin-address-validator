declare const NEMValidator: {
    /**
     * Check if an address is valid
     *
     * @param {string} _address - An address
     *
     * @return {boolean} - True if address is valid, false otherwise
     */
    isValidAddress(_address: string): boolean;
};
declare type ValidatorParams = Parameters<typeof NEMValidator.isValidAddress>;
export { NEMValidator };
export type { ValidatorParams };
