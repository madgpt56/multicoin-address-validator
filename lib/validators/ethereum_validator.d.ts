declare const ETHValidator: {
    isValidAddress(address: string): boolean;
};
declare type ValidatorParams = Parameters<typeof ETHValidator.isValidAddress>;
export { ETHValidator };
export type { ValidatorParams };
