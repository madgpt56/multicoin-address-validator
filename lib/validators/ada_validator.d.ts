import { OptsNetworkTypeOptional } from '../types';
declare type AdaCurrency = typeof import('../currencies/ada').adaCurrency;
declare type Currency = AdaCurrency;
declare type CurrencyNetworkType = keyof AdaCurrency['bech32Hrp'];
declare const ADAValidator: {
    isValidAddress(address: string, currency: Currency, opts?: OptsNetworkTypeOptional<CurrencyNetworkType>): boolean;
};
declare type ValidatorParams = Parameters<typeof ADAValidator.isValidAddress>;
export { ADAValidator };
export type { ValidatorParams };
