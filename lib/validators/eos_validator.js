"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EOSValidator = void 0;
const EOSValidator = {
    isValidAddress(address) {
        return /^[a-z0-9.]{12}$/.test(address);
    },
};
exports.EOSValidator = EOSValidator;
//# sourceMappingURL=eos_validator.js.map