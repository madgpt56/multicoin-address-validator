"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.XRPValidator = void 0;
const base_x_1 = __importDefault(require("base-x"));
const toHex_1 = require("../utils/toHex");
const sha256Checksum_1 = require("../utils/sha256Checksum");
const ALLOWED_CHARS = 'rpshnaf39wBUDNEGHJKLM4PQRST7VWXYZ2bcdeCg65jkm8oFqi1tuvAxyz';
const codec = (0, base_x_1.default)(ALLOWED_CHARS);
const regexp = new RegExp(`^r[${ALLOWED_CHARS}]{27,35}$`);
function verifyChecksum(address) {
    const bytes = codec.decode(address);
    const computedChecksum = (0, sha256Checksum_1.sha256Checksum)((0, toHex_1.toHex)(bytes.slice(0, -4)));
    const checksum = (0, toHex_1.toHex)(bytes.slice(-4));
    return computedChecksum === checksum;
}
const XRPValidator = {
    /**
     * ripple address validation
     */
    isValidAddress(address) {
        if (regexp.test(address)) {
            return verifyChecksum(address);
        }
        return false;
    },
};
exports.XRPValidator = XRPValidator;
//# sourceMappingURL=ripple_validator.js.map