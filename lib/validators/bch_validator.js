"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BCHValidator = void 0;
const base32_1 = require("../utils/base32");
const bech32_1 = require("../crypto/bech32");
const bitcoin_validator_1 = require("./bitcoin_validator");
function validateAddress(address, currency, opts) {
    const networkType = opts ? opts.networkType : '';
    let prefix = 'bitcoincash';
    const regexp = new RegExp(currency.regexp);
    let raw_address;
    const res = address.split(':');
    if (res.length === 1) {
        raw_address = address;
    }
    else {
        if (res[0] !== 'bitcoincash') {
            return false;
        }
        raw_address = res[1];
    }
    if (!regexp.test(raw_address)) {
        return false;
    }
    if (raw_address.toLowerCase() !== raw_address && raw_address.toUpperCase() !== raw_address) {
        return false;
    }
    const decoded = (0, base32_1.base32Decode)(raw_address);
    if (networkType === 'testnet') {
        prefix = 'bchtest';
    }
    try {
        if (bech32_1.bech32.verifyChecksum(prefix, decoded, bech32_1.bech32.encodings.BECH32)) {
            return false;
        }
    }
    catch (e) {
        return false;
    }
    return true;
}
const BCHValidator = {
    isValidAddress(address, currency, opts) {
        return validateAddress(address, currency, opts)
            || bitcoin_validator_1.BTCValidator.isValidAddress(address, currency, opts);
    },
};
exports.BCHValidator = BCHValidator;
//# sourceMappingURL=bch_validator.js.map