declare const EOSValidator: {
    isValidAddress(address: string): boolean;
};
declare type ValidatorParams = Parameters<typeof EOSValidator.isValidAddress>;
export { EOSValidator };
export type { ValidatorParams };
