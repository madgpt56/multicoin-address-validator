declare const XTZValidator: {
    isValidAddress(address: string): boolean;
};
declare type ValidatorParams = Parameters<typeof XTZValidator.isValidAddress>;
export { XTZValidator };
export type { ValidatorParams };
