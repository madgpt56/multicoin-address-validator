declare const XLMValidator: {
    isValidAddress(address: string): boolean;
};
declare type ValidatorParams = Parameters<typeof XLMValidator.isValidAddress>;
export { XLMValidator };
export type { ValidatorParams };
