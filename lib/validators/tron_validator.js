"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TRXValidator = void 0;
const sha256_1 = require("../utils/sha256");
const base58Decode_1 = require("../utils/base58Decode");
const byteArray2hexStr_1 = require("../utils/byteArray2hexStr");
const hexStr2byteArray_1 = require("../utils/hexStr2byteArray");
function decodeBase58Address(base58Sting) {
    if (base58Sting.length <= 4) {
        return false;
    }
    let address;
    try {
        address = (0, base58Decode_1.base58Decode)(base58Sting);
    }
    catch (e) {
        return false;
    }
    const len = address.length;
    const offset = len - 4;
    const checkSum = address.slice(offset);
    address = address.slice(0, offset);
    const hash0 = (0, sha256_1.sha256)((0, byteArray2hexStr_1.byteArray2hexStr)(address));
    const hash1 = (0, hexStr2byteArray_1.hexStr2byteArray)((0, sha256_1.sha256)(hash0));
    const checkSum1 = hash1.slice(0, 4);
    if (checkSum[0] === checkSum1[0] && checkSum[1] === checkSum1[1] && checkSum[2]
        === checkSum1[2] && checkSum[3] === checkSum1[3]) {
        return address;
    }
    return false;
}
function getEnv(currency, networkType) {
    let evn = networkType || 'prod';
    if (evn !== 'prod' && evn !== 'testnet')
        evn = 'prod';
    return currency.addressTypes[evn][0];
}
const TRXValidator = {
    /**
     * tron address validation
     */
    isValidAddress(mainAddress, currency, opts) {
        var _a;
        const networkType = (_a = opts === null || opts === void 0 ? void 0 : opts.networkType) !== null && _a !== void 0 ? _a : '';
        const address = decodeBase58Address(mainAddress);
        if (!address) {
            return false;
        }
        if (address.length !== 21) {
            return false;
        }
        return getEnv(currency, networkType) === address[0];
    },
};
exports.TRXValidator = TRXValidator;
//# sourceMappingURL=tron_validator.js.map