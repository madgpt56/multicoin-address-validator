declare const SCValidator: {
    isValidAddress(address: string): boolean;
};
declare type ValidatorParams = Parameters<typeof SCValidator.isValidAddress>;
export { SCValidator };
export type { ValidatorParams };
