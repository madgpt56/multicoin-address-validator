"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BIP173Validator = void 0;
const bech32_1 = require("../crypto/bech32");
// bip 173 bech 32 addresses (https://github.com/bitcoin/bips/blob/master/bip-0173.mediawiki)
const BIP173Validator = {
    isValidAddress(address, currency, opts = {}) {
        const { networkType = 'prod' } = opts;
        const decoded = bech32_1.bech32.decode(address, bech32_1.bech32.encodings.BECH32);
        if (!decoded) {
            return false;
        }
        const bech32Hrp = decoded.hrp;
        const correctBech32Hrps = networkType === 'prod' || networkType === 'testnet'
            ? currency.bech32Hrp[networkType]
            : currency.bech32Hrp.prod.concat(currency.bech32Hrp.testnet);
        return correctBech32Hrps.indexOf(bech32Hrp) !== -1;
    },
};
exports.BIP173Validator = BIP173Validator;
//# sourceMappingURL=bip173_validator.js.map