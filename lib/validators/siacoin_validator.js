"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SCValidator = void 0;
const blake2b_1 = require("../utils/blake2b");
function verifyChecksum(address) {
    const checksumBytes = address.slice(0, 32 * 2);
    const check = address.slice(32 * 2, 38 * 2);
    const blakeHash = (0, blake2b_1.blake2b)(checksumBytes, 32).slice(0, 6 * 2);
    return blakeHash === check;
}
const SCValidator = {
    isValidAddress(address) {
        if (address.length !== 76) {
            // Check if it has the basic requirements of an address
            return false;
        }
        // Otherwise check each case
        return verifyChecksum(address);
    },
};
exports.SCValidator = SCValidator;
//# sourceMappingURL=siacoin_validator.js.map