import { ExtractNetworkType, OptsNetworkTypeOptional } from '../types';
declare type XmrCurrency = typeof import('../currencies/xmr').xmrCurrency;
declare type LokiCurrency = typeof import('../currencies/loki').lokiCurrency;
declare type Currency = XmrCurrency | LokiCurrency;
declare type CurrencyNetworkType = ExtractNetworkType<Currency> | 'both';
declare const XMRValidator: {
    isValidAddress(address: string, currency: Currency, opts?: OptsNetworkTypeOptional<CurrencyNetworkType>): boolean;
};
declare type ValidatorParams = Parameters<typeof XMRValidator.isValidAddress>;
export { XMRValidator };
export type { ValidatorParams };
