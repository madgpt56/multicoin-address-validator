"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.USDTValidator = void 0;
const ethereum_validator_1 = require("./ethereum_validator");
const bitcoin_validator_1 = require("./bitcoin_validator");
function checkBothValidators(address, currency, opts) {
    const result = bitcoin_validator_1.BTCValidator.isValidAddress(address, currency, opts);
    return result || ethereum_validator_1.ETHValidator.isValidAddress(address);
}
const USDTValidator = {
    isValidAddress(address, currency, opts) {
        if (opts) {
            if (opts.chainType === 'erc20') {
                return ethereum_validator_1.ETHValidator.isValidAddress(address);
            }
            if (opts.chainType === 'omni') {
                return bitcoin_validator_1.BTCValidator.isValidAddress(address, currency, opts);
            }
        }
        return checkBothValidators(address, currency, opts);
    },
};
exports.USDTValidator = USDTValidator;
//# sourceMappingURL=usdt_validator.js.map