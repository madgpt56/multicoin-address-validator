declare const AlgoValidator: {
    isValidAddress(address: string): boolean;
};
declare type ValidatorParams = Parameters<typeof AlgoValidator.isValidAddress>;
export { AlgoValidator };
export type { ValidatorParams };
