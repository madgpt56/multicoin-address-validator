"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sha256Checksum = void 0;
const sha256_1 = require("./sha256");
function sha256Checksum(payload) {
    return (0, sha256_1.sha256)((0, sha256_1.sha256)(payload)).slice(0, 8);
}
exports.sha256Checksum = sha256Checksum;
//# sourceMappingURL=sha256Checksum.js.map