"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.keccak256Checksum = void 0;
const js_sha3_1 = require("js-sha3");
function keccak256Checksum(payload) {
    return (0, js_sha3_1.keccak256)(payload).toString().slice(0, 8);
}
exports.keccak256Checksum = keccak256Checksum;
//# sourceMappingURL=keccak256Checksum.js.map