"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.bigNumberToBuffer = void 0;
const buffer_1 = require("buffer");
const bignumber_js_1 = __importDefault(require("bignumber.js"));
function bigNumberToBuffer(bigNumberStr) {
    let hex = (new bignumber_js_1.default(bigNumberStr)).toString(16);
    if (hex.charAt(0) === '-') {
        throw new Error('converting negative numbers to Buffers not supported yet');
    }
    const len = Math.ceil(hex.length / 2);
    const buf = buffer_1.Buffer.alloc(len);
    // zero-pad the hex string so the chunks are all `size` long
    while (hex.length < 2 * len)
        hex = `0${hex}`;
    const hx = hex
        .split(/(.{2})/)
        .filter((s) => s.length);
    hx.forEach((chunk, i) => {
        buf[i] = parseInt(chunk.slice(0, 2), 16);
    });
    return buf;
}
exports.bigNumberToBuffer = bigNumberToBuffer;
//# sourceMappingURL=bigNumberToBuffer.js.map