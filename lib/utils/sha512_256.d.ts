import { Buffer } from 'buffer';
export declare function sha512_256(payload: Parameters<typeof Buffer.from>[0]): string;
