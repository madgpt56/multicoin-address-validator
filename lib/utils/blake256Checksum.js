"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.blake256Checksum = exports.blake256 = void 0;
const blake256_1 = require("../crypto/blake256");
function blake256(hexString) {
    return new blake256_1.Blake256().update(hexString, 'hex').digest('hex');
}
exports.blake256 = blake256;
function blake256Checksum(payload) {
    return blake256(blake256(payload)).slice(0, 8);
}
exports.blake256Checksum = blake256Checksum;
//# sourceMappingURL=blake256Checksum.js.map