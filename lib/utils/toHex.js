"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.toHex = void 0;
const numberToHex_1 = require("./numberToHex");
function toHex(arrayOfBytes) {
    let hex = '';
    for (let i = 0; i < arrayOfBytes.length; i++) {
        hex += (0, numberToHex_1.numberToHex)(arrayOfBytes[i]);
    }
    return hex;
}
exports.toHex = toHex;
//# sourceMappingURL=toHex.js.map