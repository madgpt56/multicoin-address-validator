import { Message } from 'js-sha3';
export declare function keccak256Checksum(payload: Message): string;
