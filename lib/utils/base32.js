"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.base32Decode = void 0;
const base_x_1 = __importDefault(require("base-x"));
const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567';
const bs32 = (0, base_x_1.default)(alphabet);
function base32Decode(s) {
    const sPadded = s.length % 8
        ? s.padEnd(Math.ceil(s.length / 8) * 8, alphabet[0])
        : s;
    try {
        const arr = Uint8Array.from(bs32.decode(sPadded));
        const len = (s.length * 5) / 8;
        return arr[0] === 0 ? arr.slice(1, len + 1) : arr.slice(0, len);
    }
    catch (e) {
        return new Uint8Array();
    }
}
exports.base32Decode = base32Decode;
//# sourceMappingURL=base32.js.map