"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.blake2b = void 0;
const buffer_1 = require("buffer");
const blake2b_1 = require("../crypto/blake2b");
function blake2b(hexString, outlen) {
    return new blake2b_1.Blake2b(outlen).update(buffer_1.Buffer.from(hexString, 'hex')).digest();
}
exports.blake2b = blake2b;
//# sourceMappingURL=blake2b.js.map