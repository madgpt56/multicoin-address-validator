"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.numberToHex = void 0;
function numberToHex(number, length) {
    let hex = number.toString(16);
    if (hex.length % 2 === 1) {
        hex = `0${hex}`;
    }
    return length ? hex.padStart(length, '0') : hex;
}
exports.numberToHex = numberToHex;
//# sourceMappingURL=numberToHex.js.map