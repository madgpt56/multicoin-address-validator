"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.blake2b256 = void 0;
const buffer_1 = require("buffer");
const blake2b_1 = require("../crypto/blake2b");
function blake2b256(hexString) {
    return new blake2b_1.Blake2b(32).update(buffer_1.Buffer.from(hexString, 'hex')).digest();
}
exports.blake2b256 = blake2b256;
//# sourceMappingURL=blake2b256.js.map