"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sha256x2 = void 0;
const sha256_1 = require("./sha256");
function sha256x2(buffer) {
    return (0, sha256_1.sha256)((0, sha256_1.sha256)(buffer));
}
exports.sha256x2 = sha256x2;
//# sourceMappingURL=sha256x2.js.map