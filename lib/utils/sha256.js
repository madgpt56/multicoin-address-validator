"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.sha256 = void 0;
const sha256_1 = __importDefault(require("jssha/dist/sha256"));
function sha256(payload) {
    const sha = new sha256_1.default('SHA-256', 'HEX');
    sha.update(payload);
    return sha.getHash('HEX');
}
exports.sha256 = sha256;
//# sourceMappingURL=sha256.js.map