export declare function blake256(hexString: string): string;
export declare function blake256Checksum(payload: string): string;
