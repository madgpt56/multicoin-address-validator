"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sha512_256 = void 0;
const buffer_1 = require("buffer");
const js_sha512_1 = require("js-sha512");
function sha512_256(payload) {
    const hash = js_sha512_1.sha512_256.create();
    hash.update(buffer_1.Buffer.from(payload, 'hex'));
    return hash.hex().toUpperCase();
}
exports.sha512_256 = sha512_256;
//# sourceMappingURL=sha512_256.js.map