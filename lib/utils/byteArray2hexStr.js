"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.byteArray2hexStr = void 0;
/* Convert a byte to string */
function byte2hexStr(byte) {
    const hexByteMap = '0123456789ABCDEF';
    return `${hexByteMap.charAt(byte >> 4)}${hexByteMap.charAt(byte & 0x0f)}`;
}
function byteArray2hexStr(byteArray) {
    let str = '';
    let i;
    for (i = 0; i < (byteArray.length - 1); i++) {
        str += byte2hexStr(byteArray[i]);
    }
    str += byte2hexStr(byteArray[i]);
    return str;
}
exports.byteArray2hexStr = byteArray2hexStr;
//# sourceMappingURL=byteArray2hexStr.js.map