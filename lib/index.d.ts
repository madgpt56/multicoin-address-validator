import { getSupportedSymbols, CurrencySymbolAnyRegister, CurrencyNameAnyRegister, ValidateOpts } from './currencies';
declare function validate(address: string, currencySymbolOrName: CurrencySymbolAnyRegister | CurrencyNameAnyRegister, networkTypeOrOpts?: string | ValidateOpts): any;
export { validate, getSupportedSymbols, };
